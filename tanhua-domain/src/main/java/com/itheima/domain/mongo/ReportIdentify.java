package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "report_identify")//TODO
//报告鉴定表
public class ReportIdentify implements Serializable {
    private ObjectId id;
    private String conclusion;//鉴定结果
    private String cover;//鉴定图片
    private List<Map<String, String>> dimensions;//(维度工具类，根据分数)维度:(维度项，纬度值)外向，判断，抽象，理性
}
