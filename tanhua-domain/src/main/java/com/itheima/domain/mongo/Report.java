package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "report")
//报告表
public class Report implements Serializable {
    private ObjectId id;
    private Long userId;//用户id
    private ObjectId identifyId;//鉴定表的id(鉴定表id相同为相似用户)
    private List<Long> similarYouIds;//与你相似的用户id
    private Integer scored;//分数预留功能
}
