package com.itheima.domain.mongo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "answer_records")//TODO
//回答记录表
public class AnswerRecords implements Serializable {


    private ObjectId id;
    private Long userId;//用户id
    List<Map<String, String>> answers;
}
