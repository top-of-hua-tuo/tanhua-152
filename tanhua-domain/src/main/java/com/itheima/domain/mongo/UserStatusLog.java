package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "userStatusLog")
public class UserStatusLog  implements Serializable {
    ObjectId id;
    Long adminId;//操作员管理者ID
    Long userId;//被冻结用户ID
    Integer freezingTime;//冻结时间，1为冻结3天，2为冻结7天，3为永久冻结
    Integer freezingRange;//冻结范围，1为冻结登录，2为冻结发言，3为冻结发布动态
    String reasonsForFreezing;//冻结原因
    String frozenRemarks;//冻结备注
    Integer freezingByMistake;//是否误封，进行解封操作，是为1，不是为2
    String reasonsForFreezingByMistake;
    Long created;
    Long updated;
}
