package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "voice")
public class Voice implements Serializable {
    ObjectId Id;//主键ID
    Long userId;//发布用户ID
    String voiceURL;//音频路径
    Boolean visibility;//是否被查看
    Long created;//创建时间

}
