package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
//评分表
public class MarkSheet implements Serializable {
    private Long id; //用户id
    private Integer A;//A选项
    private Integer B;//B选项
    private Integer C;//C选项
    private Integer D;//D选项
    private Integer E;//E选项
    private Integer F;//F选项
    private Integer G;//G选项
}
