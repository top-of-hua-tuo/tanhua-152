package com.itheima.domain.db;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
//回答记录表
public class Viod_AnswerRecords implements Serializable {
    //放弃,不要了 用mongo数据库
    //设置主键类型为用户输入
    @TableId(type = IdType.INPUT)
    private Long id; //用户id
    private String  t1;//试题编号
    private String  t2;//试题编号
    private String  t3;//试题编号
    private String  t4;//试题编号
    private String  t5;//试题编号
    private String  t6;//试题编号
    private String  t7;//试题编号
    private String  t8;//试题编号
    private String  t9;//试题编号
    private String  t10;//试题编号
}
