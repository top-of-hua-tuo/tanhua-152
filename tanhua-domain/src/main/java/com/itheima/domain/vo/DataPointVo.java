package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class DataPointVo implements Serializable {
    //数据点名称
    private String title;

    //数量
    private Long amount;
}
