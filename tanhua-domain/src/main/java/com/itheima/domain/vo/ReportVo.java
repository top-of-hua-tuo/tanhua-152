package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
//测灵魂-查看结果
public class ReportVo implements Serializable {
    private ObjectId id;
    private String conclusion;//鉴定结果
    private String cover;//鉴定图片
    private List<DimensionVo> dimensions;//(维度工具类，根据分数)维度:(维度项，纬度值)外向，判断，抽象，理性
    private List<SimilarYouVo> similarYou;//与你相似
}
