package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
//测灵魂-问卷列表
public class QuestionVo implements Serializable {

    private String id;//试题编号
    private String question;//题目
    private List<OptionVo> options;//选项

}
