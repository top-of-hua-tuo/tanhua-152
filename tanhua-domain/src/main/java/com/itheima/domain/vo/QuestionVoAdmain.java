package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor

//试题表
public class QuestionVoAdmain implements Serializable {
    private ObjectId id;
    private Long qid;//试题编号(idService全局唯一)
    private String question;//题目
    private List<Map<String,String>> options;//选项
    private String level;//初级，中级，高级
    private List<Map<String,String>> scores;//分数
}
