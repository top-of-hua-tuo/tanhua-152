package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
//测灵魂-问卷列表
public class OptionVo implements Serializable {
    private String id;//选项编号
    private String option;//选项

}
