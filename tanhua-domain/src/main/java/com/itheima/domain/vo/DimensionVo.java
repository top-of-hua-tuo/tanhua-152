package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
//测灵魂-查看结果
public class DimensionVo implements Serializable {
    private  String key;//维度项
    private  String value;//纬度值

}
