package com.itheima.domain.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
//问卷表
public class QuestionnaireVoAdmain implements Serializable {
    private ObjectId id;//问卷编号
    private String name;//初级灵魂题，中级灵魂题，高级灵魂题
    private String cover;//封面
    private String level;//初级，中级，高级
    private Integer star;//星别(2~5)
    private List<Map<String,String>> questions;//试题(全局唯一试题id)

}
