package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnalysisUsersVo implements Serializable {
    //本年
    private List<DataPointVo>thisYear;

    //去年
    private List<DataPointVo>lastYear;
}
