package com.itheima.domain.mongo;


import com.itheima.domain.vo.QuestionVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
//测灵魂-问卷列表
public class QuestionnaireVo implements Serializable {
    private String id;//问卷编号
    private String name;//初级灵魂题，中级灵魂题，高级灵魂题
    private String cover;//封面
    private String level;//初级，中级，高级
    private Integer star;//星别(2~5)
    private List<QuestionVo> questions;//试题
    private Integer isLock;//是否锁住(0解锁,1锁住)
    private String reportId;//最新报告id

}
