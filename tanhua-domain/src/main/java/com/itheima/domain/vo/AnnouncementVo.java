package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnnouncementVo implements Serializable {

    private String  id;  //公告id
    private String title; //公告标题
    private String description; //公告内容
    private String createDate;  //公告创建时间


}
