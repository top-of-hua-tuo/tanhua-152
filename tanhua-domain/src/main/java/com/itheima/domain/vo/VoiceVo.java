package com.itheima.domain.vo;

import com.itheima.domain.db.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoiceVo implements Serializable {
    private Long userId;//发布用户ID
    private String avatar;//头像
    private String nickname;//名称
    private String gender;//性别
    private Integer age;//年龄
    private String soundUrl;//音频路径
    private Integer remainingTimes;//剩余次数

    // 设置用户信息
    public void setUserInfo(UserInfo userInfo) {
        if (userInfo != null) {
            this.userId = userInfo.getId();
            this.avatar = userInfo.getAvatar();
            this.nickname = userInfo.getNickname();
            this.gender = userInfo.getGender();
            this.age = userInfo.getAge();
        }
    }
}
