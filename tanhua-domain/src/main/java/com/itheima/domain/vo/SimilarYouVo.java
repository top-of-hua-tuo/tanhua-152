package com.itheima.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
//测灵魂-查看结果
public class SimilarYouVo implements Serializable {
    private Integer id;//用户id
    private String avatar;//头像
}
