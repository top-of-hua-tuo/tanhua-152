package com.itheima.service.mongo;

import com.itheima.domain.mongo.Visitor;

import java.util.List;

public interface VisitorService {
    List<Visitor> findVisitorsSinceLastAccessTime(Long userId,Long lastAccessTime);
}
