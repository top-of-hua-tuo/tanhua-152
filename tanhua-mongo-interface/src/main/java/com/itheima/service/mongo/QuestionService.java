package com.itheima.service.mongo;

import com.itheima.domain.mongo.Question;
import com.itheima.domain.vo.PageBeanVo;

public interface QuestionService {


    Question findByQuestionId(Long questionId);

    PageBeanVo findByPage(Integer pageNum, Integer pageSize,String level, String keyword);

    Long save(Question question);

    void update(Question question);
}
