package com.itheima.service.mongo;

import com.itheima.domain.mongo.FocusUser;

// 关注服务
public interface FocusUserService {

    // 关注用户
    void saveFocusUser(FocusUser focusUser);
    
    // 取消关注
    void deleteFocusUser(Long userId,Long focusUserId);
}