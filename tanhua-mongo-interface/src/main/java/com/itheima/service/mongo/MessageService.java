package com.itheima.service.mongo;

public interface MessageService {
    //通过审核
    void pass(String movementId);
    //拒绝审核
    void reject(String movementId);
}
