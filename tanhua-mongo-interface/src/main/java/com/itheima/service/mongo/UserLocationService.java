package com.itheima.service.mongo;

import java.util.HashMap;
import java.util.List;

public interface UserLocationService {
    void saveOrUpdate(Long userId,double longitude,double latitude,String address);
    List<Long> searchNear(Long userId,int distance);
    //根据用户ID查询用户的地理坐标
    HashMap<String, Double> findLocationByUserId(Long userId);
}
