package com.itheima.service.mongo;

import com.itheima.domain.mongo.Questionnaire;
import com.itheima.domain.vo.PageBeanVo;

import java.util.List;

public interface QuestionnaireService {

    List<Questionnaire> findAll();

    void updateByLevel(String level, Long questionIds);

    PageBeanVo findByPage(Integer pageNum, Integer pageSize, String level, String keyword);


    Questionnaire findByName(String name);
}
