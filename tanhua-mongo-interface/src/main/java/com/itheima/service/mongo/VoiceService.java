package com.itheima.service.mongo;

import com.itheima.domain.mongo.Voice;

public interface VoiceService {
    void save(Voice voice);
    Long getVisibleCount(Long userId);
    Voice findVoiceByRandomNumber(Long randomNum,Long userId);
}
