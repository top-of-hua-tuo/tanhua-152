package com.itheima.service.mongo;

import com.itheima.domain.mongo.Report;
import org.bson.types.ObjectId;

import java.util.List;

public interface ReportService {

    Report findByUserId(Long userId);

    //查询所有相似的用户报告
    List<Report> findSimilarYouByUserId(Long userId);

    void save(Report report);
}
