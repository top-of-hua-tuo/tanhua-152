package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.vo.PageBeanVo;

public interface MovementService {
    void save(Movement movement);
    PageBeanVo findMyMovementByPage(int pageNum, int pageSize, Long userId);
    PageBeanVo findFriendMovementByPage(int pageNum, int pageSize, Long userId);
    PageBeanVo findRecommendMovementByPage(int pageNum, int pageSize, Long userId);

    Movement findById(String id);


    PageBeanVo findMovementByCondition4Page(Long uid, Integer state, int pageNum, int pageSize);

    void update(Movement movement);

    int saveCommentLike(Comment comment);
}
