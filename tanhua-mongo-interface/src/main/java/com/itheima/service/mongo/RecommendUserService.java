package com.itheima.service.mongo;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.vo.PageBeanVo;

public interface RecommendUserService {
    RecommendUser findTodayBest(Long userId);
    PageBeanVo findByPage(int pageNum,int pageSize,Long userId);
    RecommendUser findPersonal(Long recommendUserId,Long userId);
}

