package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.vo.PageBeanVo;

public interface CommentService {
    int save(Comment comment);
    int delete(Long userId,Integer commentType,String movementId);

    PageBeanVo findMovementCommentByPage(int pageNum, int pageSize, String movementId);

    PageBeanVo findByPage(int pageNum,int pageSize,int commentType,Long userId);



}

