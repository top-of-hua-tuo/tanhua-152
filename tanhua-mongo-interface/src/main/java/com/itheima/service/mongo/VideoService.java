package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Video;
import com.itheima.domain.vo.PageBeanVo;

// 视频服务
public interface VideoService {

    // 分页查询视频列表
    PageBeanVo findByPage(int pageNum, int pageSize, Long userId);

    // 保存视频
    void save(Video video);

    PageBeanVo findUserVideoByPage(int pageNum,int pageSize,Long userId);

    int saveCommentLike(Comment comment);

    PageBeanVo findVideoCommentByPage(int pageNum,int pageSize,String videoId);


}