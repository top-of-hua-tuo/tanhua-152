package com.itheima.service.mongo;

import com.itheima.domain.vo.PageBeanVo;

public interface FriendService {
    void addContact(Long userId,Long friendId);
    PageBeanVo findContactsByPage(int pageNum,int pageSize,Long userId);

    boolean isFriend(Long userId,Long friendId);
    void deleteContact(Long userId,Long friendId);
}
