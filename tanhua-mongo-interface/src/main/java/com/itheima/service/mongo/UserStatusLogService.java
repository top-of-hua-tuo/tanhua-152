package com.itheima.service.mongo;

import com.itheima.domain.mongo.UserStatusLog;
import org.bson.types.ObjectId;

public interface UserStatusLogService {
    void save(UserStatusLog userStatusLog);
    UserStatusLog findById(ObjectId userStatusLogId);
}
