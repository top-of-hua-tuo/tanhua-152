package com.itheima.service.mongo;

import com.itheima.domain.mongo.UserLike;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.domain.vo.UserLikeCountVo;

public interface UserLikeService {
    void save(UserLike userLike);

    boolean isMutualLike(Long userId,Long likeUserId);

    //删除喜欢
    void delete(Long userId,Long likeUserId);

    //各种统计
    UserLikeCountVo findEveryCount(Long userId);

    //各种详情
    PageBeanVo findEveryDetailByPage(int pageNum,int pageSize,int type,Long userId);
}
