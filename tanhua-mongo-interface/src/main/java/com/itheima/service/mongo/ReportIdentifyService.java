package com.itheima.service.mongo;

import com.itheima.domain.mongo.ReportIdentify;
import org.bson.types.ObjectId;

import java.util.List;

public interface ReportIdentifyService {
    ReportIdentify findById(ObjectId id);

    //查询所有
    List<ReportIdentify> findAll();
}
