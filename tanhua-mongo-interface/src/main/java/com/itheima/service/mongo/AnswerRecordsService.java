package com.itheima.service.mongo;

import com.itheima.domain.mongo.AnswerRecords;

public interface AnswerRecordsService {

    void save(AnswerRecords answerRecords);
}
