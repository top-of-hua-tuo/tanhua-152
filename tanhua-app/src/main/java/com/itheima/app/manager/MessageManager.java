package com.itheima.app.manager;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.im.HuanXinTemplate;
import com.itheima.domain.db.Announcement;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Friend;
import com.itheima.domain.vo.*;
import com.itheima.service.db.AnnouncementService;
import com.itheima.service.db.QuestionService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CommentService;
import com.itheima.service.mongo.FriendService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MessageManager {


    @Reference //dubbo
    UserInfoService userInfoService;

    @Reference
    QuestionService questionService;

    @Autowired
    HuanXinTemplate huanXinTemplate;

    @Reference
    FriendService friendService;

    @Reference
    CommentService commentService;

    @Reference
    AnnouncementService announcementService;

    public ResponseEntity replyStrangerQuestion(Long recommendUserId, String reply) {
        //1.获取当前登陆的id
        Long userId = UserHolder.get().getId();

        //2.调用userInfoService获取我的昵称
        String nickname = userInfoService.findById(userId).getNickname();

        //3.根据推荐用户id获取ta的陌生人问题
        Question question = questionService.findByUserId(recommendUserId);
        if (question == null) {
            question = new Question();
            question.setStrangerQuestion("你是喜欢渣男，还是暗恋靓女？？");
        }

        //4.调用环信api发送消息
        //4.1 组装信息map
        Map<String,String> map = new HashMap<>();
        map.put("userId",userId.toString());
        map.put("nickname",nickname);
        map.put("strangerQuestion",question.getStrangerQuestion());
        map.put("reply",reply);

        //4.2 发送
        huanXinTemplate.sendMsg(recommendUserId.toString(), JSON.toJSONString(map));
        return ResponseEntity.ok(null);
    }

    public  ResponseEntity addContact(Long friendId) {
        //1.获取当前登陆用户的id
        Long userId = UserHolder.get().getId();

        //2.调用friendService完成好友添加
        friendService.addContact(userId,friendId);

        //3.调用环信操作完成好友添加
        huanXinTemplate.addContacts(userId,friendId);
        return ResponseEntity.ok(null);
    }

    /*public ResponseEntity findContactsByPage(int pageNum, int pageSize) {
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.调用service查询好友 分页数据
        PageBeanVo page = friendService.findContactsByPage(pageNum, pageSize, userId);

        //3.获取好友列表,遍历它,获取好友info,放入一个新集合中
        //3.0 创建UserInfoList
        List<UserInfo> userInfoList = new ArrayList<>();

        //3.1 获取好友列表,遍历它
        List<Friend> items = (List<Friend>) page.getItems();
        if (CollUtil.isNotEmpty(items)) {
            for (Friend friend : items) {
                //3.2 查询好友的userInfo
                UserInfo userInfo = userInfoService.findById(friend.getFriendId());

                //3.3 将userInfo放入list中
                userInfoList.add(userInfo);

            }
        }

        //4.给分页对象设置新集合
        page.setItems(userInfoList);
        //5.返回分页对象
        return ResponseEntity.ok(page);
    }*/

    public ResponseEntity findContactsByPage(int pageNum, int pageSize) {
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.调用service,查询当前页分页数据
        PageBeanVo page = friendService.findContactsByPage(pageNum, pageSize, userId);

        //3.获取分页数据中的好友id,查询其UserInfo
        List<Friend> items = (List<Friend>) page.getItems();
        //3.0 创建一个ContactVoList
        List<ContactVo> contactVoList = new ArrayList<>();

        //3.1 遍历集合,获取每个朋友id,查询其UserInfo,
        if (CollUtil.isNotEmpty(items)) {
            for (Friend item : items) {
                //3.2 查询好友详情
                UserInfo userInfo = userInfoService.findById(item.getFriendId());

                //3.3 封装contactVo
                ContactVo contactVo = new ContactVo();
                contactVo.setUserInfo(userInfo);
                contactVo.setUserId(item.getFriendId().toString());

                //3.4 将vo放入集合中
                contactVoList.add(contactVo);
            }
        }

        //4.给page设置封装后的集合
        page.setItems(contactVoList);

        return ResponseEntity.ok(page);

    }

    public ResponseEntity findCommentsByPage(int pageNum, int pageSize, int commentType) {
        //1.获取当期用户id
        Long userId = UserHolder.get().getId();

        //2.调用service,分页查询指定类型的评论数据
        PageBeanVo page = commentService.findByPage(pageNum, pageSize, commentType, userId);

        //3.遍历评论集合,获取评论人id,查询UserInfo,封装UserCommentVo,放入新集合中
        //3.0 创建一个UserCommentVoList
        List<UserCommentVo> userCommentVoList = new ArrayList<>();

        //3.1 遍历评论集合
        List<Comment> items = (List<Comment>) page.getItems();
        if (CollUtil.isNotEmpty(items)) {
            for (Comment comment : items) {
                //3.2 获取评论人id
                Long id = comment.getUserId();

                //3.3 查询UserInfo
                UserInfo userInfo = userInfoService.findById(id);

                //3.4 封装UserCommentVo,放入新集合中
                UserCommentVo userCommentVo = new UserCommentVo();

                userCommentVo.setAvatar(userInfo.getAvatar());
                userCommentVo.setNickname(userInfo.getNickname());
                userCommentVo.setCreateDate(DateUtil.format(new Date(comment.getCreated()),"yyyy-MM-dd HH:mm:ss"));

                userCommentVo.setId(comment.getPublishId().toString());//待定

                userCommentVoList.add(userCommentVo);
            }
        }

        //4.给page设置新集合
        page.setItems(userCommentVoList);

        //5.返回
        return ResponseEntity.ok(page);
    }

    public ResponseEntity findAnnouncement(int pageNum, int pageSize) {

        PageBeanVo pageBeanVo = announcementService.findAll(pageNum, pageSize);

        List<Announcement> items = (List<Announcement>) pageBeanVo.getItems();
        List<AnnouncementVo> list = new ArrayList<>();
        if (CollUtil.isNotEmpty(items)){
            for (Announcement item : items) {
                //创建Vo
                AnnouncementVo announcementVo = new AnnouncementVo();

                //封装Vo
                announcementVo.setId(String.valueOf(item.getId()));
                announcementVo.setCreateDate(DateUtil.format(item.getCreated(),"yyyy-MM-dd HH-mm-ss"));
                announcementVo.setDescription(item.getDescription());
                announcementVo.setTitle(item.getTitle());
                //将Vo加入LIST中
                list.add(announcementVo);

            }
        }

        pageBeanVo.setItems(list);
        return ResponseEntity.ok(pageBeanVo);
    }
}
