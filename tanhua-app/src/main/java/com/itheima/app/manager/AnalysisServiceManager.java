package com.itheima.app.manager;

import com.itheima.domain.vo.AnalysisUsersVo;
import com.itheima.service.db.AnalysisApiService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class AnalysisServiceManager {
    @Reference
    AnalysisApiService analysisApiService;
    public ResponseEntity users(Long sd,Long ed,Integer type){
        AnalysisUsersVo vo = analysisApiService.users(sd,ed,type);
        return ResponseEntity.ok(vo);
    }
}
