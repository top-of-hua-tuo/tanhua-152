package com.itheima.app.manager;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.Notification;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.User;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.vo.ErrorResult;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.domain.vo.SettingVo;
import com.itheima.service.db.BlackListService;
import com.itheima.service.db.NotificationService;
import com.itheima.service.db.QuestionService;
import com.itheima.service.db.UserService;
import com.itheima.util.ConstantUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;

@Service //spring
public class SettingManager {

    @Reference //dubbo
    QuestionService questionService;

    @Reference
    NotificationService notificationService;

    @Reference
    BlackListService blackListService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Reference
    UserService userService;

    public ResponseEntity findSetting() {
        //1.获取用户id
        Long userId = UserHolder.get().getId();

        //2.调用questionService查询问题
        Question question = questionService.findByUserId(userId);

        //3.调用notificationService查询 通知设置
        Notification notification = notificationService.findByUserId(userId);

        //4.创建SettingVo
        SettingVo settingVo = new SettingVo();

        //5.给vo设置问题
        if (question != null) {
            settingVo.setStrangerQuestion(question.getStrangerQuestion());
        }

        //6.给vo设置 用户设置
        if (notification != null) {
            settingVo.setPinglunNotification(notification.getPinglunNotification());
            settingVo.setLikeNotification(notification.getLikeNotification());
            settingVo.setGonggaoNotification(notification.getGonggaoNotification());
        }

        //7. 给vo设置用户id
        settingVo.setId(userId);
        return ResponseEntity.ok(settingVo);
    }

    public ResponseEntity setQuestion(String content) {
        //1.获取用户id
        Long userId = UserHolder.get().getId();

        //2.通过用户id查询陌生人问题
        Question question = questionService.findByUserId(userId);

        //3.判断问题对象是否为null
        if (question == null) {
            //3.1若为null,添加
            question = new Question();
            question.setUserId(userId);
            question.setStrangerQuestion(content);

            questionService.save(question);
        }else {
            //3.2若不为null,修改
            question.setStrangerQuestion(content);

            questionService.update(question);
        }
        return ResponseEntity.ok(null);
    }

    public ResponseEntity setNotification(Notification notificationParam) {
        //1.获取用户id
        Long userId = UserHolder.get().getId();

        //2.通过用户id获取推送通知
        Notification notification = notificationService.findByUserId(userId);

        //3.判断对象是否为null
        if (notification == null) {
            //3.1若为空,保存
            notificationParam.setUserId(userId);

            notificationService.save(notificationParam);
        }else {
            //3.2 否则,更新
            notificationParam.setId(notification.getId());

            notificationService.update(notificationParam);
        }
        return ResponseEntity.ok(null);
    }

    public ResponseEntity findBlackListByPage(int pageNum, int pageSize) {
        //1.获取当前用户的id
        Long userId = UserHolder.get().getId();

        //2.调用service查询当前页的数据 Page
        Page<UserInfo> page = blackListService.findByPage(pageNum, pageSize, userId);

        //3.创建PageBeanVo对象
        //4.将 page对象中的数据设置到vo对象中
        PageBeanVo pageBeanVo = new PageBeanVo(page);

        //5.返回vo对象
        return ResponseEntity.ok(pageBeanVo);
    }

    public ResponseEntity deleteBlackList(Long blackId) {
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.调用service删除当前用户的一个黑名单
        blackListService.deleteByUserIdAndBlackId(userId,blackId);

        return ResponseEntity.ok(null);
    }

    //修改手机号之发送短信验证码
    public ResponseEntity sendVerificationCode() {

        //获取用户id作为凭证
        //Long userId = UserHolder.get().getId();
        // TODO  产品上线前，开启短信发送
        // 1.生成验证码
        // String smsCode = RandomUtil.randomNumbers(6);
        String smsCode = "123456";

        // 2.发送短信
        // smsTemplate.sendSms(phone, smsCode);

        // 3.将验证码存入redis
        stringRedisTemplate.opsForValue().set(ConstantUtil.SMS_CODE , smsCode, Duration.ofMinutes(5));

        return ResponseEntity.ok(null); // 返回状态码200，响应体空
    }

    //修改手机号之校验验证码
    public ResponseEntity checkVerificationCode(String verificationCode) {

        Boolean verification=true;

        //用户id
        //Long userId = UserHolder.get().getId();

        //从redis中获取
        String redisCode = stringRedisTemplate.opsForValue().get(ConstantUtil.SMS_CODE );

        if(!StrUtil.equals(verificationCode,redisCode)){
            //对比不正确,设为false;
            verification=false;
        }
        //对比正确,清楚验证码
        stringRedisTemplate.delete(ConstantUtil.SMS_CODE);
        HashMap<String,Boolean> resultMap = new HashMap<>();
        resultMap.put("verification", verification);
        return ResponseEntity.ok(resultMap);
    }

    //修改手机号
    public ResponseEntity updatePhone(String phone) {

        User user = new User();
        user.setPhone(phone);
        user.setId(UserHolder.get().getId());
        userService.updatePhone(user);
        return ResponseEntity.ok(null);
    }
}
