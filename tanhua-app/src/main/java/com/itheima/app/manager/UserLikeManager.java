package com.itheima.app.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.im.HuanXinTemplate;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.UserLike;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.domain.vo.UserLikeVo;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.FriendService;
import com.itheima.service.mongo.UserLikeService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserLikeManager {

    @Reference //dubbo
    UserLikeService userLikeService;

    @Reference
    FriendService friendService;

    @Autowired
    HuanXinTemplate huanXinTemplate;

    @Reference
    UserInfoService userInfoService;

    public ResponseEntity tanhuaLove(Long likeUserId) {
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.封装UserLike对象
        UserLike userLike = new UserLike();
        userLike.setUserId(userId);
        userLike.setLikeUserId(likeUserId);
        userLike.setCreated(System.currentTimeMillis());

        //3.调用service完成保存操作
        userLikeService.save(userLike);

        //4.判断是否为相互喜欢,若是的话
        if (userLikeService.isMutualLike(userId,likeUserId)) {
            //4.1在friend表中保存双方关系
            friendService.addContact(userId,likeUserId);
            //4.2在环信中添加好友
            huanXinTemplate.addContacts(userId,likeUserId);
        }

        return ResponseEntity.ok(null);
    }

    public ResponseEntity tanhuaUnLove(Long likeUserId) {
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.判断他们是否为好友,若是的话
        if (friendService.isFriend(userId,likeUserId)) {
            //2.1 删除friend表中的好友关系
            friendService.deleteContact(userId,likeUserId);
            //2.2 删除环信中好友关系
            huanXinTemplate.deleteContacts(userId,likeUserId);
        }

        //3.删除userLike中记录
        userLikeService.delete(userId,likeUserId);
        return ResponseEntity.ok(null);
    }

    public ResponseEntity findEveryCount() {
        return ResponseEntity.ok(userLikeService.findEveryCount(UserHolder.get().getId()));
    }


    public ResponseEntity findEveryDetailByPage(int pageNum, int pageSize, int type) {
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.调用service完成查询操作,返回分页对象
        PageBeanVo page = userLikeService.findEveryDetailByPage(pageNum, pageSize, type, userId);

        //3.获取分页中UserLikeVo集合
        List<UserLikeVo> items = (List<UserLikeVo>) page.getItems();

        //4.遍历集合,获取用户id,查询UserInfo,将UserInfo封装到UserLikeVo中
        if (CollUtil.isNotEmpty(items)) {
            for (UserLikeVo userLikeVo : items) {
                //查询用户的userInfo
                UserInfo userInfo = userInfoService.findById(userLikeVo.getId());

                //封装vo
                BeanUtil.copyProperties(userInfo,userLikeVo);

                //判断用户是否相互喜欢
                if (userLikeService.isMutualLike(userId,userLikeVo.getId())) {
                    //设置为已经喜欢过
                    userLikeVo.setAlreadyLove(true);
                }
            }
        }

        //5.返回分页对象
        return ResponseEntity.ok(page);
    }
}
