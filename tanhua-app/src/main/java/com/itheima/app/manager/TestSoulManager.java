package com.itheima.app.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.MarkSheet;
import com.itheima.domain.db.Viod_AnswerRecords;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.*;
import com.itheima.domain.mongo.QuestionnaireVo;
import com.itheima.domain.vo.*;
import com.itheima.service.db.MarkSheetService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.*;
import com.itheima.util.ComputeUtil;
import com.itheima.util.ConstantUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TestSoulManager {
    @Reference
    QuestionnaireService questionnaireService;
    @Reference
    QuestionService questionService;
    @Reference
    ReportService reportService;
    @Reference
    ReportIdentifyService reportIdentifyService;
    @Reference
    UserInfoService userInfoService;
    @Reference
    AnswerRecordsService answerRecordsService;
    @Reference
    MarkSheetService markSheetService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;




    //测灵魂-问卷列表
    public ResponseEntity findQuestionnaireList() {
        //1.获取用户id
        Long userId = UserHolder.get().getId();

        //2.调用问卷service查询问卷数组(只有试题id)
        List<Questionnaire> questionnaires = questionnaireService.findAll();

        //3.遍历问卷数组,封装问卷vo返回
        String level = null;//获取问卷等级，判断使用
        List<QuestionnaireVo> questionnaireVos = new ArrayList<>();
        if (CollUtil.isNotEmpty(questionnaires)) {
            for (Questionnaire questionnaire : questionnaires) {

                //3.1获取试题id数组
                List<Long> questionIds = questionnaire.getQuestionIds();
                List<QuestionVo> questionVos = new ArrayList<>();//试题数组

                //3.2调用questionService,查询问题
                if (CollUtil.isNotEmpty(questionIds)) {
                    for (Long questionId : questionIds) {
                        Question question = questionService.findByQuestionId(questionId);//试题
                        level = question.getLevel();
                        //3.4遍历选项数组封装成optionVo
                        List<OptionVo> optionVos = new ArrayList<>();//选项数组vo
                        List<Map<String, String>> options = question.getOptions();//A早晨，B晚上
                        if (CollUtil.isNotEmpty(options)) {
                            for (Map<String, String> option : options) {
                                Set<String> keySet = option.keySet();
                                for (String key : keySet) {//外围判断了，内层可省空指针判断
                                    OptionVo optionVo = new OptionVo();
                                    optionVo.setId(key);//例如:A
                                    optionVo.setOption(option.get(key));//早晨
                                    optionVos.add(optionVo);
                                }
                            }
                        }


                        //3.3试题封装成questionVo
                        QuestionVo questionVo = new QuestionVo();
                        questionVo.setId(question.getQid().toString());//试题唯一id
                        questionVo.setQuestion(question.getQuestion());
                        questionVo.setOptions(optionVos);
                        questionVos.add(questionVo);
                    }
                }

                //3.5封装问卷vo
                QuestionnaireVo questionnaireVo = new QuestionnaireVo();//创建VO对象
                BeanUtil.copyProperties(questionnaire, questionnaireVo);//对象对拷
                questionnaireVo.setQuestions(questionVos);//试题数组单独设置

                //3.6调用报告service查询报告id,给vo的报告id赋值
                Report report = reportService.findByUserId(userId);

                //3.7 TODO  到redis中查询是否需要上锁(0解锁,1锁住)
                String redisLevel = stringRedisTemplate.opsForValue().get(StrUtil.format(ConstantUtil.SOUL_LEVEL, userId));
                /*回答了初级:解锁初级和中级，设置初级最新报告日志
                * 回答了中级：解锁全部，设置初级和中级最新报告日志
                * 回答了高级：解锁全部，设置初级、中级、高级最新报告日志
                * */
                if (ConstantUtil.SOUL_HIGH.equals(redisLevel)){
                    questionnaireVo.setReportId(report.getIdentifyId().toString());//设置vo的报告鉴定id
                    //已经答完中级或高级，全部解锁
                    questionnaireVo.setIsLock(0);
                }else if ( ConstantUtil.SOUL_MIDDLE.equals(redisLevel)
                ) {
                    if (level.equals(ConstantUtil.SOUL_MIDDLE)||level.equals(ConstantUtil.SOUL_LOW)  ) {
                        questionnaireVo.setReportId(report.getIdentifyId().toString());//设置vo的报告鉴定id
                    }
                    //已经答完中级或高级，全部解锁
                    questionnaireVo.setIsLock(0);
                }else   if (ConstantUtil.SOUL_LOW.equals(redisLevel)) {
                   //已经答完初级，解锁初级和中级
                   if (level.equals(ConstantUtil.SOUL_LOW) ) {
                       questionnaireVo.setIsLock(0);
                       questionnaireVo.setReportId(report.getIdentifyId().toString());//设置vo的报告鉴定id
                   } else if (level.equals(ConstantUtil.SOUL_MIDDLE)) {
                       questionnaireVo.setIsLock(0);
                   } else {
                       questionnaireVo.setIsLock(1);
                   }

               } else {
                    //没有回答过，解锁初级
                    if (level.equals(ConstantUtil.SOUL_LOW)) {
                        questionnaireVo.setIsLock(0);
                    } else {
                        questionnaireVo.setIsLock(1);
                    }
                }



                questionnaireVos.add(questionnaireVo);//存放到vo数组
            }
        }

        return ResponseEntity.ok(questionnaireVos);
    }

    //测灵魂-查看结果
    public ResponseEntity findReport(String identifyId) {
        //1.调用鉴定报告表service查询鉴定信息
        ReportIdentify reportIdentify = reportIdentifyService.findById(new ObjectId(identifyId));

        //2.封装ReportVo
        //2.1获取维度集合转换成维度Vo集合(获取维护vo集合)
        List<Map<String, String>> dimensions = reportIdentify.getDimensions();

        List<DimensionVo> dimensionVos = new ArrayList<>();
        if (CollUtil.isNotEmpty(dimensions)) {
            for (Map<String, String> dimension : dimensions) {
                Set<String> keySet = dimension.keySet();
                for (String key : keySet) {//外围判断了，内层可省空指针判断
                    DimensionVo dimensionVo = new DimensionVo();
                    dimensionVo.setKey(key);
                    dimensionVo.setValue(dimension.get(key));
                    dimensionVos.add(dimensionVo);
                }
            }
        }


        //2.2获取相似用户id集合转换成用户详细信息Vo集合(获取相似用户Vo集合)
        Long userId = UserHolder.get().getId();
        Report report = reportService.findByUserId(userId);
        List<Long> similarYouIds = report.getSimilarYouIds();
        //2.3遍历相似用户id
        List<SimilarYouVo> similarYouVos = new ArrayList<>();
        if (CollUtil.isNotEmpty(similarYouIds)) {
            for (Long similarYouId : similarYouIds) {
                //2.4获取用户详细信息
                UserInfo userInfo = userInfoService.findById(similarYouId);
                SimilarYouVo similarYouVo = new SimilarYouVo();

                //2.5封装成similarYouVo
                similarYouVo.setId(similarYouId.intValue());


                //用户还没有填写详细信息的话就设置个默认的头像
                similarYouVo.setAvatar(userInfo!=null?userInfo.getAvatar():"https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/tanhua/avatar_11.jpg");
                similarYouVos.add(similarYouVo);//加入相似用户vo集合
            }
        }

        //4.3封装ReportVo的鉴定表部分
        ReportVo reportVo = new ReportVo();
        reportVo.setConclusion(reportIdentify.getConclusion());
        reportVo.setCover(reportIdentify.getCover());
        reportVo.setDimensions(dimensionVos);
        reportVo.setSimilarYou(similarYouVos);

        return ResponseEntity.ok(reportVo);
    }

    //测灵魂-提交问卷
    public ResponseEntity saveReport(List<Map<String, String>> answerList) {
        //0.获取用户id
        Long userId = UserHolder.get().getId();

        //1.保存answerList保存到mongo表中
        AnswerRecords answerRecords = new AnswerRecords();
        answerRecords.setUserId(userId);
        answerRecords.setAnswers(answerList);
        answerRecordsService.save(answerRecords);


        //2.解析answerList,从tb_mark_sheet(评分表中查询分数)
        Integer scoreCount=0;//定义分数统计器
        String questionId=null;//定义问卷id(用于查询本问卷的等级)
        String optionId = null;//定义选择项id
        if (CollUtil.isNotEmpty(answerList)) {
            for (Map<String, String> answerMap : answerList) {
                Set<String> keySet = answerMap.keySet();
                Integer tempCount = 0;//临时计数器
                for (String key : keySet) {
                    //查询分数(试题编号,选项编号)
                    if (tempCount % 2 == 0) {
                        questionId = answerMap.get(key);
                        tempCount++;
                    } else {
                        optionId = answerMap.get(key);
                    }
                }

                Integer score = markSheetService.findScore(questionId,optionId);
                scoreCount += score;
            }
        }

        //3.封装report报告表
        //3.1工具类获取鉴定表index
        Integer identifyIndex = ComputeUtil.getIdentifyIndex(scoreCount);

        //3.2调用报告service查询所有相似用户
        List<Long> similarYouIds = new ArrayList<>();
        List<Report> reportSimilarYouList= reportService.findSimilarYouByUserId(userId);//与你相似的报告集合
        //遍历获取与你相似的用户id,存储到集合中
        if (CollUtil.isNotEmpty(reportSimilarYouList)) {
            for (Report reportSimilarYou : reportSimilarYouList) {
                Long similarYouId = reportSimilarYou.getUserId();
                similarYouIds.add(similarYouId);
            }
        }

        //3.4封装report报告表
        Report report = new Report();
        report.setUserId(userId);
        ObjectId identifyId = reportIdentifyService.findAll().get(identifyIndex).getId();//先所有再根据index获取
        report.setIdentifyId(identifyId);
        report.setSimilarYouIds(similarYouIds);
        report.setScored(scoreCount);

        reportService.save(report);

        //4.设置标志到redis表(解锁),从试题表中查询问卷等级并设置到redis中
        Question question = questionService.findByQuestionId(Long.parseLong(questionId));
        String level = question.getLevel();

        stringRedisTemplate.opsForValue().set(StrUtil.format(ConstantUtil.SOUL_LEVEL, userId), level);


        return ResponseEntity.ok(identifyId.toString());
    }
}
