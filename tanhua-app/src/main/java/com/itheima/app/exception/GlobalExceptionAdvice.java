package com.itheima.app.exception;

import com.itheima.domain.vo.ErrorResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice //全局异常处理注解
public class GlobalExceptionAdvice {

    @ExceptionHandler(Exception.class)//处理所有异常
    public ResponseEntity exceptionHandler(Exception ex){
        //打印异常信息
        ex.printStackTrace();

        //TODO 将异常信息写入日志

        return ResponseEntity.status(500).body(ErrorResult.error());
    }
}
