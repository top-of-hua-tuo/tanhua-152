package com.itheima.app.job;

import com.itheima.util.ConstantUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class TaoHuaJob {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    //每天清除用户查看桃花传音的剩余次数
    @Scheduled(cron = "0 0 0 1/1 * ? ")
    public void deleteRemainingTimesFromRedis(){
        Set<String> keys = stringRedisTemplate.keys(ConstantUtil.TaoHua_remainingTimes + "*");

        for (String key : keys) {
            stringRedisTemplate.delete(key);
        }

        System.out.println("恢复用户查看桃花传音的剩余次数");
    }
}
