package com.itheima.app.controller;

import com.itheima.app.manager.MovementManager;
import com.itheima.domain.mongo.Movement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
public class MovementController {

    @Autowired
    MovementManager movementManager;

    //发布动态
    @PostMapping("/movements")
    public ResponseEntity save(Movement movement, MultipartFile[] imageContent) throws IOException {
        return movementManager.save(movement,imageContent);
    }

    //查询我的动态
    @GetMapping("/movements/all")
    public ResponseEntity findMyMovementByPage(
            @RequestParam(value = "page",defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8") int pageSize,
            Long userId){
        return movementManager.findMyMovementByPage(pageNum,pageSize,userId);
    }

    //查询好友动态
    @GetMapping("/movements")
    public ResponseEntity findFriendMovementByPage(
            @RequestParam(value = "page",defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8") int pageSize){
        return movementManager.findFriendMovementByPage(pageNum,pageSize);
    }


    //查询推荐动态
    @GetMapping("/movements/recommend")
    public ResponseEntity findRecommendMovementByPage(
            @RequestParam(value = "page",defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8") int pageSize){

        return movementManager.findRecommendMovementByPage(pageNum,pageSize);
    }

    //点赞
    @GetMapping("/movements/{movementId}/like")
    public ResponseEntity saveMovementLike(@PathVariable String movementId){
        return movementManager.saveMovementLike(movementId);
    }

    //取消点赞
    @GetMapping("/movements/{movementId}/dislike")
    public ResponseEntity deleteMovementLike(@PathVariable String movementId){
        return movementManager.deleteMovementLike(movementId);
    }

    //喜欢
    @GetMapping("/movements/{movementId}/love")
    public ResponseEntity saveMovementLove(@PathVariable String movementId){
        return movementManager.saveMovementLove(movementId);
    }

    //取消喜欢
    @GetMapping("/movements/{movementId}/unlove")
    public ResponseEntity deleteMovementLove(@PathVariable String movementId){
        return movementManager.deleteMovementLove(movementId);
    }

    //查询单个动态详情
    @GetMapping("/movements/{movementId}")
    public  ResponseEntity findById(@PathVariable String movementId){
        return movementManager.findById(movementId);
    }

    //分页查询动态评论
    @GetMapping("/comments")
    public ResponseEntity findMovementCommentByPage(
            @RequestParam(value = "page",defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8") int pageSize,
            String movementId){

        return movementManager.findMovementCommentByPage(pageNum,pageSize,movementId);
    }

    //发表评论
    @PostMapping("/comments")
    public ResponseEntity saveMovementComment(@RequestBody Map<String,String> map){
        //1.获取动态id 和 评论内容
        String movementId = map.get("movementId");
        String content = map.get("comment");

        //2.调用manager完成保存操作
        return movementManager.saveMovementComment(movementId,content);
    }

    //评论点赞
    @GetMapping("/comments/{commentId}/like")
    public ResponseEntity saveCommentLike(@PathVariable String commentId){
        return movementManager.saveCommentLike(commentId);
    }

}
