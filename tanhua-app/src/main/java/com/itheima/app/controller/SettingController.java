package com.itheima.app.controller;

import com.itheima.app.manager.SettingManager;
import com.itheima.domain.db.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class SettingController {

    @Autowired
    SettingManager settingManager;

    @GetMapping("/users/settings")
    public ResponseEntity findSetting(){
        return settingManager.findSetting();
    }

    @PostMapping("/users/questions")
    public ResponseEntity setQuestion(@RequestBody Map<String,String> map){
        //1.获取问题内容
        String content = map.get("content");

        //2.调用manager完成操作
        return settingManager.setQuestion(content);
    }

    @PostMapping("/users/notifications/setting")
    public ResponseEntity setNotification(@RequestBody Notification notificationParam){
        return settingManager.setNotification(notificationParam);
    }

    @GetMapping("/users/blacklist")
    public ResponseEntity findBlackListByPage(
            @RequestParam(value = "page",defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8") int pageSize){

        return settingManager.findBlackListByPage(pageNum,pageSize);
    }

    @DeleteMapping("/users/blacklist/{uid}")
    public ResponseEntity deleteBlackList(@PathVariable("uid") Long blackId){
        return settingManager.deleteBlackList(blackId);
    }

    //修改手机号之发送短信验证码
    @PostMapping("/users/phone/sendVerificationCode")
    public ResponseEntity sendVerificationCode(){
        return settingManager.sendVerificationCode();
    }

    //修改手机号之校验验证码
    @PostMapping("/users/phone/checkVerificationCode")
    public ResponseEntity checkVerificationCode(@RequestBody Map<String,String>map){
        String verificationCode = map.get("verificationCode");
        return settingManager.checkVerificationCode(verificationCode);
    }

    //修改手机号
    @PostMapping("/users/phone")
    public ResponseEntity updatePhone(@RequestBody Map<String,String>map){
        String phone = map.get("phone");
        return settingManager.updatePhone(phone);
    }
}
