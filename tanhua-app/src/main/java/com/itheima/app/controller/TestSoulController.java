package com.itheima.app.controller;

import com.itheima.app.manager.TestSoulManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class TestSoulController {
    @Autowired
    TestSoulManager testSoulManager;

    //测灵魂-问卷列表
    @GetMapping("/testSoul")
    public ResponseEntity findQuestionnaireList() {
        //调用manager完成查询操作
        return testSoulManager.findQuestionnaireList();
    }

    //测灵魂-查看结果
    @GetMapping("/testSoul/report/{id}")
    public ResponseEntity findReport(@PathVariable(value = "id") String identifyId) {
        //调用manager完成查询操作
        return testSoulManager.findReport(identifyId);

    }

    //测灵魂-提交问卷
    @PostMapping("/testSoul")
    public ResponseEntity saveReport(@RequestBody Map<String,List<Map<String,String>>> answers) {
        //获取前台参数
        List<Map<String, String>> answerList = answers.get("answers");
        //调用manager完成保存操作
        return testSoulManager.saveReport(answerList);
    }
}
