package com.itheima.app.controller;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.VideoManager;
import com.itheima.domain.vo.PageBeanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
public class VideoController {

    @Autowired
    VideoManager videoManager;

    @GetMapping("/smallVideos")
    public ResponseEntity findRecommendVideoByPage(
            @RequestParam(defaultValue = "1",value = "page") int pageNum,
            @RequestParam(defaultValue = "8",value = "pagesize") int pageSize){
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();

        //2.调用manager完成分页操作,返回pageBeanVo
        PageBeanVo page = videoManager.findRecommendVideoByPage(pageNum,pageSize,userId);

        //3.返回ResponseEntity
        return ResponseEntity.ok(page);
    }

    @PostMapping("/smallVideos")
    public ResponseEntity save(MultipartFile videoThumbnail,MultipartFile videoFile) throws IOException {
        //1.接受参数
        //2.获取当前用户的id
        Long userId = UserHolder.get().getId();

        //3.调用manager完成保存
        videoManager.save(videoThumbnail,videoFile,userId);

        //4.返回ResponseEntity
        return ResponseEntity.ok(null);
    }

    @PostMapping("/smallVideos/{focusUserId}/userFocus")
    public ResponseEntity focusUser(@PathVariable Long focusUserId){
        return videoManager.focusUser(focusUserId);
    }

    @PostMapping("/smallVideos/{focusUserId}/userUnFocus")
    public ResponseEntity UnFocusUser(@PathVariable Long focusUserId){
        return videoManager.unFocusUser(focusUserId);
    }

    //视频点赞
    @PostMapping("/smallVideos/{videoId}/like")
    public ResponseEntity saveVideoLike(@PathVariable String videoId){
        return videoManager.saveVideoLike(videoId);
    }

    //视频评论发布
    @PostMapping("/smallVideos/{videoId}/comments")
    public ResponseEntity saveVideoComment(@PathVariable String videoId,
                                           @RequestBody Map<String,String> map){
        //获取评论内容
        String content = map.get("comment");
        return videoManager.saveVideoComment(videoId,content);
    }

    //视频评论列表
    @GetMapping("/smallVideos/{videoId}/comments")
    public ResponseEntity findVideoCommentByPage(@PathVariable String videoId,
             @RequestParam(value = "page",defaultValue = "1") Integer pageNum ,
             @RequestParam(value = "pagesize",defaultValue = "8") Integer pageSize ){
        return videoManager.findVideoCommentByPage(videoId,pageNum,pageSize);
    }

}
