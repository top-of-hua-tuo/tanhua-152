package com.itheima.app.controller;

import com.itheima.app.manager.MakeFriendManager;
import com.itheima.domain.vo.PageBeanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@RestController
public class MakeFriendController {

    @Autowired
    MakeFriendManager makeFriendManager;

    //今日最佳
    @GetMapping("/tanhua/todayBest")
    public ResponseEntity findTodayBest(){
        return makeFriendManager.findTodayBest();
    }

    //推荐好友列表
    @GetMapping("/tanhua/recommendation")
    public ResponseEntity findRecommendUserByPage(
            @RequestParam(value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8")int pageSize
    ){
        return makeFriendManager.findRecommendUserByPage(pageNum,pageSize);
    }

    //推荐用户详情
    @GetMapping("/tanhua/{recommendUserId}/personalInfo")
    public ResponseEntity findRecommendUserPersonal(@PathVariable Long recommendUserId){
        return makeFriendManager.findRecommendUserPersonal(recommendUserId);
    }

    //查看陌生人问题
    @GetMapping("/tanhua/strangerQuestions")
    public ResponseEntity findStrangerQuestion(Long userId){
        return makeFriendManager.findStrangerQuestion(userId);
    }

    //查询最近访客
    @GetMapping("/movements/visitors")
    public ResponseEntity findVisitorsSinceLastAccessTime(){
        return makeFriendManager.findVisitorsSinceLastAccessTime();
    }

    @PostMapping("/baidu/location")
    public ResponseEntity saveUserLocation(@RequestBody Map<String,Object> map){
        //1.获取经纬度和地址
        double longitude = (double) map.get("longitude");
        double latitude = (double) map.get("latitude");
        String address = (String) map.get("addrStr");


        System.out.println("最新的地址为:"+address);
        //2.调用manager完成保存操作
        return makeFriendManager.saveUserLocation(longitude,latitude,address);
    }

    //搜附近
    @GetMapping("/tanhua/search")
    public ResponseEntity searchNear(String gender,int distance){
        return makeFriendManager.searchNear(gender,distance);
    }

    //探花卡片
    @GetMapping("/tanhua/cards")
    public ResponseEntity tanhuaCards(){
        //调用之前写好的操作,查询推荐用户列表
        ResponseEntity resp = makeFriendManager.findRecommendUserByPage(1, 20);
        PageBeanVo page = (PageBeanVo) resp.getBody();
        return ResponseEntity.ok( page.getItems());
    }

    //桃花传音--发布
    @PostMapping("/peachblossom")
    public ResponseEntity saveTaoHuaVoice(@RequestBody MultipartFile soundFile) throws IOException {
        return makeFriendManager.saveTaoHuaVoice(soundFile);
    }

    //桃花传音--获取
    // TODO 未测试!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @GetMapping("/peachblossom")
    public ResponseEntity findTaoHuaVoice(){
        return makeFriendManager.findTaoHuaVoice();
    }

}
