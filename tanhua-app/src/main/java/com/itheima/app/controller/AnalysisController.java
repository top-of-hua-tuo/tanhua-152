package com.itheima.app.controller;

import com.itheima.app.manager.AnalysisServiceManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnalysisController {
    @Autowired
    AnalysisServiceManager analysisServiceManager;



    @GetMapping("/management/dashboard/users")
    public ResponseEntity users(Long sd,Long ed,Integer type){
        return analysisServiceManager.users(sd,ed,type);
    }



}
