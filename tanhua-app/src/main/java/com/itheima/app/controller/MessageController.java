package com.itheima.app.controller;

import cn.hutool.core.util.NumberUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.MessageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MessageController {

    @Autowired
    MessageManager messageManager;

    //环信用户认证
    @GetMapping("/huanxin/user")
    public ResponseEntity findHuanxinUser(){
        //1.获取当前用户id
        Long userId = UserHolder.get().getId();
        //2.创建一个map,封装id和password
        Map<String,String> map = new HashMap<>();
        map.put("username",userId.toString());
        map.put("password","123456");

        //3.响应回去
        return ResponseEntity.ok(map);
    }

    @PostMapping("/tanhua/strangerQuestions")
    public ResponseEntity replyStrangerQuestion(@RequestBody Map<String,String>map){
        //获取推荐用户的id和回复的内容
        Long recommendUserId = NumberUtil.parseLong( map.get("userId"));
        String reply = map.get("reply");

        //调用manager完成操作
        return messageManager.replyStrangerQuestion(recommendUserId,reply);
    }

    //添加联系人
    @PostMapping("/messages/contacts")
    public ResponseEntity addContact(@RequestBody Map<String,Long> map){
        return messageManager.addContact(map.get("userId"));
    }

    //查询好友列表
    @GetMapping("/messages/contacts")
    public ResponseEntity findContactsByPage(
            @RequestParam(value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8")int pageSize){
        return messageManager.findContactsByPage(pageNum,pageSize);
    }

    //查询点赞
    @GetMapping("/messages/likes")
    public ResponseEntity findLikesByPage(
            @RequestParam(value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8")int pageSize){
        return messageManager.findCommentsByPage(pageNum,pageSize,1);
    }

    //查询评论
    @GetMapping("/messages/comments")
    public ResponseEntity findCommentsByPage(
            @RequestParam(value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8")int pageSize){
        return messageManager.findCommentsByPage(pageNum,pageSize,2);
    }

    //查询喜欢
    @GetMapping("/messages/loves")
    public ResponseEntity findLovesByPage(
            @RequestParam(value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8")int pageSize){
        return messageManager.findCommentsByPage(pageNum,pageSize,3);
    }

    //公告
    @GetMapping("/messages/announcements")
    public ResponseEntity findAnnouncement(
            @RequestParam (value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pageSize",defaultValue = "8")int pageSize){
       return messageManager.findAnnouncement(pageNum, pageSize);

    }
}
