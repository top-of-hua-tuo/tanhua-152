package com.itheima.app.controller;

import com.itheima.app.manager.UserLikeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserLikeController {

    @Autowired
    UserLikeManager userLikeManager;

    //喜欢
    @GetMapping("/tanhua/{likeUserId}/love")
    public ResponseEntity tanhuaLove(@PathVariable Long likeUserId){
        return userLikeManager.tanhuaLove(likeUserId);
    }

    //不喜欢
    @GetMapping("/tanhua/{likeUserId}/unlove")
    public ResponseEntity tanhuaUnLove(@PathVariable Long likeUserId){
        return userLikeManager.tanhuaUnLove(likeUserId);
    }

    //各种用户数量统计
    @GetMapping("/users/counts")
    public ResponseEntity findEveryCount(){
        return userLikeManager.findEveryCount();
    }

    //各种用户详情统计
    @GetMapping("/users/friends/{type}")
    public ResponseEntity findEveryDetailByPage(
            @RequestParam(value = "page",defaultValue = "1")int pageNum,
            @RequestParam(value = "pagesize",defaultValue = "8")int pageSize,
            @PathVariable int type){
        return userLikeManager.findEveryDetailByPage(pageNum,pageSize,type);
    }

    //取消喜欢
    @DeleteMapping("/users/like/{likeUserId}")
    public ResponseEntity usersUnlove(@PathVariable Long likeUserId){
        return userLikeManager.tanhuaUnLove(likeUserId);
    }

    //喜欢粉丝
    @PostMapping("/users/fans/{likeUserId}")
    public ResponseEntity usersFansLove(@PathVariable Long likeUserId){
        return userLikeManager.tanhuaLove(likeUserId);
    }
}
