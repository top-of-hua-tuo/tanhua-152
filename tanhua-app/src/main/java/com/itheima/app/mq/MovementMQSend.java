package com.itheima.app.mq;

import com.itheima.service.mongo.MovementService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MovementMQSend {
    //针对动态的操作
    public static final Integer MOVEMENT_PUBLISH = 1;// 发动态
    public static final Integer MOVEMENT_BROWSE = 2;// 浏览动态
    public static final Integer MOVEMENT_LIKE = 3;// 点赞
    public static final Integer MOVEMENT_LOVE = 4;// 喜欢
    public static final Integer MOVEMENT_COMMENT = 5;// 评论
    public static final Integer MOVEMENT_DISLIKE = 6;// 取消点赞
    public static final Integer MOVEMENT_DISLOVE = 7;// 取消喜欢

    @Autowired
    RocketMQTemplate rocketMQTemplate;

    @Reference
    MovementService movementService;


    /*
        userId:当前用户的id
        通过movementId就可以获取pid
        type:用户操作的类型
     */
    public void movementMQSend(Long userId,String movementId,Integer type){
        //1.组装map
        Map<String,String> map = new HashMap<>();
        map.put("userId",userId.toString());
        map.put("pid",movementService.findById(movementId).getPid().toString());
        map.put("type",type.toString());

        //2.发送数据到mq
        rocketMQTemplate.convertAndSend("recommond-movement",map);
    }
}
