package com.itheima.app.config;

import com.itheima.app.interceptor.TokenInterceptor;
import com.itheima.app.interceptor.UserStatusInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration //配置类
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    TokenInterceptor tokenInterceptor;
    @Autowired
    UserStatusInterceptor userStatusInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/**")//设置拦截路径
                .excludePathPatterns("/user/login", "/user/loginVerification");//排除和注册相关的路径

        registry.addInterceptor(userStatusInterceptor)
                .addPathPatterns("/user/loginReginfo", "/movements", "/comments");
    }
}
