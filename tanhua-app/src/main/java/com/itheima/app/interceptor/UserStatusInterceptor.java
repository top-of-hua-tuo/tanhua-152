package com.itheima.app.interceptor;

import cn.hutool.core.util.StrUtil;
import com.itheima.domain.mongo.UserStatusLog;
import com.itheima.service.mongo.UserStatusLogService;
import com.itheima.util.ConstantUtil;
import org.apache.dubbo.config.annotation.Reference;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserStatusInterceptor implements HandlerInterceptor {

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Reference
    UserStatusLogService userStatusLogService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断是否被冻结

        if (stringRedisTemplate.hasKey(ConstantUtil.BeFreezing_user + UserHolder.get().getId())) {
            String userStatusLogIdStr = stringRedisTemplate.opsForValue().get(ConstantUtil.BeFreezing_user + UserHolder.get().getId());
            ObjectId userStatusLogId = new ObjectId(userStatusLogIdStr);
            UserStatusLog userStatusLog = userStatusLogService.findById(userStatusLogId);
            Integer freezingRange = userStatusLog.getFreezingRange();

            String path = request.getContextPath();
            if (StrUtil.equals(path, "/user/loginReginfo") && freezingRange == ConstantUtil.FREEZING_RANGE_LOGIN) {
                return false;
            }
            if (StrUtil.equals(path, "/movements") && freezingRange == ConstantUtil.FREEZING_RANGE_MOVEMENT) {
                return false;
            }
            if (StrUtil.equals(path, "/comments") && freezingRange == ConstantUtil.FREEZING_RANGE_MESSAGE) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
