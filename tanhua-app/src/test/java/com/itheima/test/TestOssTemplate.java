package com.itheima.test;

import com.itheima.autoconfig.oss.OssTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestOssTemplate {

    @Autowired
    OssTemplate ossTemplate;

    @Test
    public void test0() throws FileNotFoundException {
        String url = ossTemplate.upload("51.jpg", new FileInputStream("D:\\pic\\51.jpg"));
        System.err.println(url);
    }
}
