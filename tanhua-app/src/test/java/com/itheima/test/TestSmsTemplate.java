package com.itheima.test;

import com.itheima.autoconfig.sms.SmsTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestSmsTemplate {

    @Autowired
    SmsTemplate smsTemplate;

    @Test
    public void test0() {
        smsTemplate.sendSms("15324020888","112233");
    }
}
