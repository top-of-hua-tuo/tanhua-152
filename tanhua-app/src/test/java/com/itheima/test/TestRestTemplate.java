package com.itheima.test;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class TestRestTemplate {
    @Test
    public void test0() {
        //restTemplate 模拟浏览器向其他应用发送请求
        RestTemplate restTemplate = new RestTemplate();

        //声明请求路径url
        String url = "http://a1.easemob.com/1160210617153018/tanhua-152/token";

        //封装请求体
        Map requestBody = new HashMap();
        requestBody.put("grant_type","client_credentials");
        requestBody.put("client_id","YXA64PAQNpA9RxKoVS5WwlqCnA");
        requestBody.put("client_secret","YXA6eWWwsgyD2ts9buhe46uh55QQEwo");

        Map responseMap = restTemplate.postForObject(url, requestBody, Map.class);

        System.err.println(responseMap);
    }
}
