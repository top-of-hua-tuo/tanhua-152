package com.itheima.test;

import cn.hutool.core.io.FileUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestFastDfs {

    @Autowired
    FastFileStorageClient client;//客户端

    @Autowired
    FdfsWebServer server;//服务器配置对象

    @Test
    public void test0() throws FileNotFoundException {

        File file = new File("E:\\152\\08.小视频&缓存\\资料\\一栗小莎子.mp4");
        FileInputStream is = new FileInputStream(file);

        //上传文件
        /*
            is:文件输入流
            size:文件的长度
            extName:文件后缀名
            metaData:文件属性集合 一般设置为null
         */
        StorePath storePath = client.uploadFile(is, file.length(), FileUtil.extName(file), null);
        System.out.println("文件相对路径:"+storePath.getFullPath());

        //获取绝对路径
        String url = server.getWebServerUrl()+storePath.getFullPath();
        System.out.println("文件绝对路径:"+url);
    }
}
