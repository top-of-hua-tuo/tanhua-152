package com.itheima.test;

import cn.hutool.core.io.FileUtil;
import com.baidu.aip.face.AipFace;
import com.baidu.aip.util.Base64Util;
import com.itheima.autoconfig.aip.AipFaceTemplate;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestAipfaceTemplate {

    @Autowired
    AipFaceTemplate aipFaceTemplate;

    @Test
    public void test1() {
        System.err.println(aipFaceTemplate.detect(FileUtil.readBytes("D:\\pic\\22.jpeg")));
    }


    @Test
    public void test0() {
        AipFace client = new AipFace("24414409", "p3kKqWI9hQaIGjAiOt817Hsr", "oIWO8fy55dFgq6iu9AR5OFVgTvWtNz8P");

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("face_field", "age");
        options.put("max_face_num", "2");
        options.put("face_type", "LIVE");
        options.put("liveness_control", "LOW");

        byte[] bytes = FileUtil.readBytes("D:\\pic\\60.jpg");
        String image = Base64Util.encode(bytes);
        String imageType = "BASE64";

        // 人脸检测
        JSONObject res = client.detect(image, imageType, options);
        System.out.println(res.toString(2));
    }
}
