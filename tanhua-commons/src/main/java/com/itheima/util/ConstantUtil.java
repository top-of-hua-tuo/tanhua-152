package com.itheima.util;

//常量定义
public class ConstantUtil {
    //===================================系统使用=====================================//
    //初始化密码
    public static final String INIT_PASSWORD = "123456";
    //jwt加密盐
    public static final String JWT_SECRET = "tanhua";


    //===================================redis中使用===================================//
    //短信验证码
    public static final String SMS_CODE = "sms:";
    //token
    public static final String USER_TOKEN = "token:";
    //动态点赞
    public static final String MOVEMENT_LIKE = "movement_like:{}_{}";
    //评论点赞
    public static final String COMMENT_LIKE = "comment_like:{}_{}";
    //动态喜欢
    public static final String MOVEMENT_LOVE = "movement_love:{}_{}";
    //视频点赞
    public static final String VIDEO_LIKE = "video_like:{}_{}";
    //视频评论
    public static final String VIDEO_COMMENT = "video_like:{}_{}";
    //关注用户
    public static final String FOCUS_USER = "focus_user:{}_{}";
    //用户最后一次访问时间
    public static final String LAST_ACCESS_TIME = "last_access_time:";
    //用户桃花传音剩余次数
    public static final String TaoHua_remainingTimes="TaoHua_remainingTimes:";
    //用户被冻结
    public static final String BeFreezing_user="beFreezing_user:";

    //===================================redis中使用======================================//
    //管理员登录验证码
    public static final String ADMIN_CODE = "admin_code_";
    //管理员令牌
    public static final String ADMIN_TOKEN = "admin_token_";


    //===================================mongodb中使用===================================//
    //视频大数据id标识
    public static final String VIDEO_ID = "video";
    //动态大数据id标识
    public static final String MOVEMENT_ID = "movement";
    //用户大数据id标识
    public static final String USER_ID = "user";


    //我的动态表名前缀
    public static final String MOVEMENT_MINE = "movement_mine_";
    //好友动态表名前缀
    public static final String MOVEMENT_FRIEND = "movement_friend_";

    //冻结时间
    public static final Integer FREEZING_TIME_THREEDAYS=1;
    public static final Integer FREEZING_TIME_ONEWEEK=2;
    public static final Integer FREEZING_TIME_ONEMONTH=3;
    //冻结范围
    public static final Integer FREEZING_RANGE_LOGIN=1;
    public static final Integer FREEZING_RANGE_MESSAGE=2;
    public static final Integer FREEZING_RANGE_MOVEMENT=3;
    //是否进行解封操作
    public static final Integer FREEZING_BY_MISTAKE_TRUE=1;
    public static final Integer FREEZING_BY_MISTAKE_FALSE=2;

    //测灵魂问卷登记
    public static final Integer TESTSOUL_QUESTIONNAIRE_LEVEL_PRIMARY=1;
    public static final Integer TESTSOUL_QUESTIONNAIRE_LEVEL_MIDDLE=2;
    public static final Integer TESTSOUL_QUESTIONNAIRE_LEVEL_HIGH=3;

    //鉴定类型ObjectId
    public static final Integer SOUL_OWL = 0;
    public static final Integer SOUL_RABBIT = 1;
    public static final Integer SOUL_FOX = 2;
    public static final Integer SOUL_LION = 3;

    //已完成测试标志
    public static final String SOUL_LEVEL = "soul_level:{}_{}";

    //问卷等级
    public static final String SOUL_LOW= "初级";
    public static final String SOUL_MIDDLE= "中级";
    public static final String SOUL_HIGH= "高级";
}
