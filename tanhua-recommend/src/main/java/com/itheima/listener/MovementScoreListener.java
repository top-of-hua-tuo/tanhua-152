package com.itheima.listener;

import cn.hutool.core.util.NumberUtil;
import com.itheima.domain.mongo.MovementScore;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RocketMQMessageListener(consumerGroup = "consumer-recommend-movement-group",topic = "recommond-movement")
public class MovementScoreListener implements RocketMQListener<Map>{

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void onMessage(Map map) {
        System.out.println("从mq中取出来用户行为数据:"+map);

        //1.从map中获取数据
        Long userId = NumberUtil.parseLong((String)map.get("userId"));
        Long pid = NumberUtil.parseLong((String)map.get("pid"));
        Integer type = NumberUtil.parseInt((String)map.get("type"));

        //2.组装成存入mongo的对象
        MovementScore movementScore = new MovementScore();

        movementScore.setUserId(userId);
        movementScore.setPublishId(pid);
        movementScore.setDate(System.currentTimeMillis());

        switch (type) {
            case 1: {
                movementScore.setScore(20d);
                break;
            }
            case 2: {
                movementScore.setScore(1d);
                break;
            }
            case 3: {
                movementScore.setScore(5d);
                break;
            }
            case 4: {
                movementScore.setScore(8d);
                break;
            }
            case 5: {
                movementScore.setScore(10d);
                break;
            }
            case 6: {
                movementScore.setScore(-5d);
                break;
            }
            case 7: {
                movementScore.setScore(-8d);
                break;
            }
        }

        //3.存入mongo
        mongoTemplate.save(movementScore);
    }
}
