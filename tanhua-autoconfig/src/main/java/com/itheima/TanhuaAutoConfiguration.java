package com.itheima;

import com.itheima.autoconfig.aip.AipFaceProperties;
import com.itheima.autoconfig.aip.AipFaceTemplate;
import com.itheima.autoconfig.huawei.HuaWeiUGCProperties;
import com.itheima.autoconfig.huawei.HuaWeiUGCTemplate;
import com.itheima.autoconfig.im.HuanXinProperties;
import com.itheima.autoconfig.im.HuanXinTemplate;
import com.itheima.autoconfig.oss.OssProperties;
import com.itheima.autoconfig.oss.OssTemplate;
import com.itheima.autoconfig.sms.SmsProperties;
import com.itheima.autoconfig.sms.SmsTemplate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({
        SmsProperties.class, //加载短信配置
        OssProperties.class,//加载oss配置
        AipFaceProperties.class,//加载ai配置
        HuanXinProperties.class,//加载环信 im 配置
        HuaWeiUGCProperties.class//加载华为云审核
})
public class TanhuaAutoConfiguration {
    @Bean
    public SmsTemplate smsTemplate(SmsProperties smsProperties) {
        return new SmsTemplate(smsProperties);
    }

    @Bean
    public OssTemplate ossTemplate(OssProperties ossProperties){
        return new OssTemplate(ossProperties);
    }

    @Bean
    public AipFaceTemplate aipFaceTemplate(AipFaceProperties aipFaceProperties) {
        return new AipFaceTemplate(aipFaceProperties);
    }

    @Bean
    public HuanXinTemplate huanXinTemplate(HuanXinProperties huanXinProperties){
        return new HuanXinTemplate(huanXinProperties);
    }

    @Bean
    public HuaWeiUGCTemplate huaWeiUGCTemplate(HuaWeiUGCProperties huaWeiUGCProperties) {
        return new HuaWeiUGCTemplate(huaWeiUGCProperties);
    }

}