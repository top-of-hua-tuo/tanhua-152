package com.itheima;

import com.itheima.service.mongo.impl.IdService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestIDService {

    @Autowired
    IdService idService;

    @Test
    public void test0() {
        Long userId = idService.getNextId("user");
        System.out.println(userId);
    }
}
