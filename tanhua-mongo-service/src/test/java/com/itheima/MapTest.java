package com.itheima;

import cn.hutool.core.collection.CollUtil;

import com.itheima.domain.mongo.Questionnaire;
import com.itheima.domain.mongo.Question;
import com.itheima.domain.mongo.ReportIdentify;
import com.itheima.service.mongo.impl.IdService;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MapTest {
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    IdService idService;

   /* @Test
    public void testSave() {
        Question questions = new Question();
        questions.setId(new ObjectId("60eaae820e5ca42fa401ea66"));
        questions.setQuestion("尝试就完事了！");
        Map<String, String> option = new HashMap<>();
        option.put("A", "能行么？");
        option.put("B", "看看吧！");
        List< Map<String, String>> options = new ArrayList<>();
        options.add(option);
        questions.setOptions(options);
        mongoTemplate.save(questions);
    }*/

   /* @Test
    public void testFind() {
        List<Question> questions = mongoTemplate.find(new Query(), Question.class);
        if (CollUtil.isNotEmpty(questions)) {
            for (Question question : questions) {
                System.out.println(question.getQuestion());
                List<Map<String, String>> options = question.getOptions();
                if (CollUtil.isNotEmpty(options)) {
                    for (Map<String, String> option : options) {
                        Set<String> keySet = option.keySet();
                        for (String s : keySet) {
                            System.out.println("选项"+s+":"+option.get(s));
                        }
                    }
                }
                System.out.println("-------------------");
            }
        }
    }*/

    @Test//问卷初始化
    public void testInitQuestionnaire() {

        Questionnaire questionnaire = new Questionnaire();//问卷
        //1.查询试题
        List<Question> questions = mongoTemplate.find(new Query(), Question.class);
        List<Long> questionIds = new ArrayList<>();
        if (CollUtil.isNotEmpty(questions)) {
            for (Question question : questions) {
                questionIds.add(question.getQid());
            }
        }
        //2.设置questionnaire
        questionnaire.setName("初级灵魂题");
        questionnaire.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/qn_cover_01.png");
        questionnaire.setLevel("初级");
        questionnaire.setStar(2);
        questionnaire.setQuestionIds(questionIds);
        //questionnaire.setIsLock(0);
        //questionnaire.setReportId("1");
        mongoTemplate.save(questionnaire);

    }

    @Test//mongoDB初始化
    public void testInitQuestion() {


        for (int i = 0; i < 3; i++) {
            String level = null;
            if (i == 0) {
                level = "初级";
            } else if (i == 1) {
                level = "中级";
            } else if (i == 2) {
                level = "高级";
            }

            //1.设置questions
            Question question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("你何时感觉最好？");
            Map<String, String> option = new HashMap<>();
            option.put("A", "早晨");
            option.put("B", "下午及傍晚");
            option.put("C", "夜里");
            List<Map<String, String>> options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);

            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("你走路时是？");
            option = new HashMap<>();
            option.put("A", "大步地快走");
            option.put("B", "小步地块走");
            option.put("C", "不快，仰着头面对着世界");
            option.put("D", "不快，低着头");
            option.put("E", "很慢");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("和人说话时？");
            option = new HashMap<>();
            option.put("A", "手臂交叠站着");
            option.put("B", "双手紧握着");
            option.put("C", "一只手或两只手放在臀部");
            option.put("D", "碰着或推着与你说话的人");
            option.put("E", "玩着你的耳朵，摸着你的下巴或用手整理头发");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("坐着休息时？");
            option = new HashMap<>();
            option.put("A", "两膝盖并拢");
            option.put("B", "两腿交叉");
            option.put("C", "两腿伸直");
            option.put("D", "一腿蜷在身下");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("碰到你感到发笑的事时，你的反应是");
            option = new HashMap<>();
            option.put("A", "一个欣赏的大笑");
            option.put("B", "笑着");
            option.put("C", "轻声地笑");
            option.put("C", "羞怯的微笑");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);



            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("当你去一个派对或社交场合时");
            option = new HashMap<>();
            option.put("A", "很大声地入场以引起注意");
            option.put("B", "安静地入场，找你认识的人");
            option.put("C", "非常安静地入场，尽量保持不被注意");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("当你非常专心工作时，有人打断你，你会");
            option = new HashMap<>();
            option.put("A", "欢迎他");
            option.put("B", "感到非常愤怒");
            option.put("C", "在上述两极端之间");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("下列颜色中，你最喜欢哪一种颜色？");
            option = new HashMap<>();
            option.put("A", "红或橘色");
            option.put("B", "黑色");
            option.put("C", "黄色或浅蓝色");
            option.put("D", "绿色");
            option.put("E", "深蓝色或紫色");
            option.put("F", "白色");
            option.put("G", "棕色或灰色");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("临入睡的前几分钟，你在床上的姿势是？");
            option = new HashMap<>();
            option.put("A", "仰躺，伸直");
            option.put("B", "俯躺，伸直");
            option.put("C", "侧躺，微蜷");
            option.put("D", "头睡在一手臂上");
            option.put("E", "被子盖过头");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            question = new Question();//试题
            question.setQid(idService.getNextId("question"));
            question.setQuestion("你经常梦到自己在");
            option = new HashMap<>();
            option.put("A", "落下");
            option.put("B", "打架或挣扎");
            option.put("C", "找东西或人");
            option.put("D", "飞或漂浮");
            option.put("E", "你平常不做梦");
            option.put("F", "你的梦都是愉快的");
            options = new ArrayList<>();
            options.add(option);
            question.setOptions(options);
            question.setLevel(level);
            mongoTemplate.save(question);


            Questionnaire questionnaire = new Questionnaire();//问卷
            //1.查询试题
            Query query = new Query().skip(i * 10).limit(10);
            List<Question> questions = mongoTemplate.find(query, Question.class);
            List<Long> questionIds = new ArrayList<>();
            if (CollUtil.isNotEmpty(questions)) {
                for (Question question1 : questions) {
                    questionIds.add(question1.getQid());
                }
            }

            //2.设置questionnaire
            questionnaire.setName(level + "灵魂题");
            questionnaire.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/qn_cover_0" + (i + 1) + ".png");
            questionnaire.setLevel(level);
            questionnaire.setStar(i + 2);
            questionnaire.setStar(i + 1);
            questionnaire.setQuestionIds(questionIds);
            //questionnaire.setIsLock(0);
            //questionnaire.setReportId("1");
            mongoTemplate.save(questionnaire);

        }


        //鉴定报告初始化
        ReportIdentify reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("猫头鹰：他们的共同特质为重计划、条理、细节精准。在行为上，表现出喜欢理性思考与分析、较重视制度、结构、规范。他们注重执行游戏规则、循规蹈矩、巨细靡遗、重视品质、敬业负责。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/owl.png");
        Map<String, String> dimension = new HashMap<>();
        dimension.put("外向", "90%");
        dimension.put("判断", "80%");
        dimension.put("抽象", "70%");
        dimension.put("理性", "90%");
        List<Map<String, String>> dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


        reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("白兔型：平易近人、敦厚可靠、避免冲突与不具批判性。在行为上，表现出不慌不忙、冷静自持的态度。他们注重稳定与中长程规划，现实生活中，常会反思自省并以和谐为中心，即使面对困境，亦能泰然自若，从容应付。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/rabbit.png");
        dimension = new HashMap<>();
        dimension.put("外向", "90%");
        dimension.put("判断", "70%");
        dimension.put("抽象", "60%");
        dimension.put("理性", "80%");
        dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


        reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("狐狸型 ：人际关系能力极强，擅长以口语表达感受而引起共鸣，很会激励并带动气氛。他们喜欢跟别人互动，重视群体的归属感，基本上是比较「人际导向」。由于他们富同理心并乐于分享，具有很好的亲和力，在服务业、销售业、传播业及公共关系等领域中，狐狸型的领导者都有很杰出的表现。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/fox.png");
        dimension = new HashMap<>();
        dimension.put("外向", "90%");
        dimension.put("判断", "80%");
        dimension.put("抽象", "60%");
        dimension.put("理性", "70%");
        dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


        reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("狮子型：性格为充满自信、竞争心强、主动且企图心强烈，是个有决断力的领导者。一般而言，狮子型的人胸怀大志，勇于冒险，看问题能够直指核心，并对目标全力以赴。\n他们在领导风格及决策上，强调权威与果断，擅长危机处理，此种性格最适合开创性与改革性的工作。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/lion.png");
        dimension = new HashMap<>();
        dimension.put("外向", "80%");
        dimension.put("判断", "80%");
        dimension.put("抽象", "90%");
        dimension.put("理性", "90%");
        dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


    }


    /*@Test//鉴定表初始化
    public void testReportIdentify() {
        ReportIdentify reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("猫头鹰：他们的共同特质为重计划、条理、细节精准。在行为上，表现出喜欢理性思考与分析、较重视制度、结构、规范。他们注重执行游戏规则、循规蹈矩、巨细靡遗、重视品质、敬业负责。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/owl.png");
        Map<String, String> dimension = new HashMap<>();
        dimension.put("外向", "90%");
        dimension.put("判断", "80%");
        dimension.put("抽象", "70%");
        dimension.put("理性", "90%");
        List<Map<String, String>> dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


        reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("白兔型：平易近人、敦厚可靠、避免冲突与不具批判性。在行为上，表现出不慌不忙、冷静自持的态度。他们注重稳定与中长程规划，现实生活中，常会反思自省并以和谐为中心，即使面对困境，亦能泰然自若，从容应付。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/rabbit.png");
        dimension = new HashMap<>();
        dimension.put("外向", "90%");
        dimension.put("判断", "70%");
        dimension.put("抽象", "60%");
        dimension.put("理性", "80%");
        dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


        reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("狐狸型 ：人际关系能力极强，擅长以口语表达感受而引起共鸣，很会激励并带动气氛。他们喜欢跟别人互动，重视群体的归属感，基本上是比较「人际导向」。由于他们富同理心并乐于分享，具有很好的亲和力，在服务业、销售业、传播业及公共关系等领域中，狐狸型的领导者都有很杰出的表现。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/fox.png");
        dimension = new HashMap<>();
        dimension.put("外向", "90%");
        dimension.put("判断", "80%");
        dimension.put("抽象", "60%");
        dimension.put("理性", "70%");
        dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


        reportIdentify = new ReportIdentify();
        reportIdentify.setConclusion("狮子型：性格为充满自信、竞争心强、主动且企图心强烈，是个有决断力的领导者。一般而言，狮子型的人胸怀大志，勇于冒险，看问题能够直指核心，并对目标全力以赴。\n他们在领导风格及决策上，强调权威与果断，擅长危机处理，此种性格最适合开创性与改革性的工作。");
        reportIdentify.setCover("https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/lion.png");
        dimension = new HashMap<>();
        dimension.put("外向", "80%");
        dimension.put("判断", "80%");
        dimension.put("抽象", "90%");
        dimension.put("理性", "90%");
        dimensions = new ArrayList<>();
        dimensions.add(dimension);
        reportIdentify.setDimensions(dimensions);
        mongoTemplate.save(reportIdentify);


    }*/


}
