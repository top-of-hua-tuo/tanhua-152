package com.itheima.service.mongo.impl;

import cn.hutool.core.util.RandomUtil;
import com.itheima.domain.mongo.Report;
import com.itheima.domain.mongo.ReportIdentify;
import com.itheima.service.mongo.ReportService;
import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Report findByUserId(Long userId) {
        //1.创建条件对象
        Report report = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), Report.class);
        //2.新用户可能查询不到就随机一个
       /* if (report==null) {
        if (report==null) {
            report = new Report();
            report.setUserId(userId);
            List<ReportIdentify> reportIdentifies = mongoTemplate.find(new Query(), ReportIdentify.class);
            report.setIdentifyId(reportIdentifies.get(0).getId());
            List<Long> similarYouIds = new ArrayList<>();
            Collections.addAll(similarYouIds, 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L);
            report.setSimilarYouIds(similarYouIds);
            report.setScored(RandomUtil.randomInt(20,60));
        }*/

        return report;
    }


    //查询所有相似的用户报告
    @Override
    public List<Report> findSimilarYouByUserId(Long userId) {
        //1.先获取自己鉴定报告表id
        Report report = findByUserId(userId);
        ObjectId identifyId = report.getIdentifyId();

        //2.根据条件查询返回，开启分页，根据分数排序(排除自己)
        Query query = new Query(Criteria.where("identifyId").is(identifyId).and("userId").ne(userId))
                .skip(0).limit(10).with(Sort.by(Sort.Order.desc("scored")));
        return mongoTemplate.find(query, Report.class);

    }

    @Override
    public void save(Report report) {
        //1.查询原先是否存在报告,存在则删除
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(report.getUserId())), report.getClass());
        //2.保存
        mongoTemplate.save(report);
    }
}
