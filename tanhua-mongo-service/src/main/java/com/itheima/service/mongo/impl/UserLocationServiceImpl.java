package com.itheima.service.mongo.impl;

import cn.hutool.core.collection.CollUtil;
import com.itheima.domain.mongo.UserLocation;
import com.itheima.service.mongo.UserLocationService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service //dubbo
public class UserLocationServiceImpl implements UserLocationService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void saveOrUpdate(Long userId, double longitude, double latitude, String address) {
        //1.通过用户id查询用户地址
        UserLocation userLocation = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), UserLocation.class);

        //2.判断用户地址是否存在
        if (userLocation == null) {
            //2.1 若不存在保存地址
            userLocation = new UserLocation();

            userLocation.setUserId(userId);
            userLocation.setLocation(new GeoJsonPoint(longitude,latitude));//经度,纬度
            userLocation.setAddress(address);
            userLocation.setCreated(System.currentTimeMillis());
            userLocation.setUpdated(System.currentTimeMillis());
            userLocation.setLastUpdated(System.currentTimeMillis());

            mongoTemplate.save(userLocation);
        }else{
            //2.2 若存在则更新
            userLocation.setLocation(new GeoJsonPoint(longitude,latitude));//经度,纬度
            userLocation.setAddress(address);
            userLocation.setLastUpdated(userLocation.getUpdated());
            userLocation.setUpdated(System.currentTimeMillis());

            mongoTemplate.save(userLocation);
        }
    }

    @Override
    public List<Long> searchNear(Long userId, int distance) {
        //1.获取我的地址
        UserLocation userLocation = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId)), UserLocation.class);

        //2.以我的地址为圆心,画个圈
        Distance dis = new Distance(distance/1000, Metrics.KILOMETERS);//设置半径 单位:km
        Circle circle = new Circle(userLocation.getLocation(),dis);//画圈

        Query query = new Query(
                Criteria.where("location").withinSphere(circle)//以我的地址为圆心,画个圈
                .and("userId").ne(userId)//排除自己
        );

        //3.查询附近的人(排除自己)
        List<UserLocation> userLocations = mongoTemplate.find(query, UserLocation.class);
        //4.遍历附近的人集合,只要用户id
        //4.1 创建一个id集合
        List<Long> ids = new ArrayList<>();

        //4.2 遍历用户集合,获取用户id,放入id集合中
        if (CollUtil.isNotEmpty(userLocations)) {
            for (UserLocation location : userLocations) {
                ids.add(location.getUserId());
            }
        }
        //5.返回id的集合
        return ids;
    }

    //根据用户的ID查询地理坐标
    @Override
    public HashMap<String, Double> findLocationByUserId(Long userId) {
        //创建查询条件
        Query query = new Query(
                Criteria.where("userId").is(userId)
        );
        //开始查询
        UserLocation userLocation = mongoTemplate.findOne(query, UserLocation.class);
        //System.out.println(userLocation);
        //解析出经纬度
        GeoJsonPoint geoJsonPoint = userLocation.getLocation();
        //封装MAP
        HashMap<String, Double> map = new HashMap<>();
        map.put("lng",geoJsonPoint.getX());
        map.put("lat",geoJsonPoint.getY());
        //返回经纬度坐标
        return map;
    }
}
