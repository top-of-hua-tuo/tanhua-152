package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.service.mongo.CommentService;
import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;


@Service //dubbo
public class CommentServiceImpl implements CommentService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public int save(Comment comment) {
        //1.查询动态所属的用户id
        ObjectId movementId = comment.getPublishId();
        Movement movement = mongoTemplate.findById(movementId, Movement.class);
        Long userId = movement.getUserId();

        comment.setPublishUserId(userId);

        //2.保存到评论表中
        mongoTemplate.save(comment);

        //3.更新动态表中该操作的数据 ++
        if (comment.getCommentType() == 1) {
            //点赞
            movement.setLikeCount(movement.getLikeCount() + 1);
        }else if (comment.getCommentType() == 2){
            //评论
            movement.setCommentCount(movement.getCommentCount() + 1);
        }else if (comment.getCommentType() == 3){
            //喜欢
            movement.setLoveCount(movement.getLoveCount() + 1);
        }

        mongoTemplate.save(movement);
        
        //4.查询最新的操作数量返回
        if (comment.getCommentType() == 1) {
            //点赞
            return  movement.getLikeCount();
        }else if (comment.getCommentType() == 2){
            //评论
            return movement.getCommentCount();
        }else if (comment.getCommentType() == 3){
            //喜欢
            return movement.getLoveCount();
        }
        return 0;
    }

    @Override
    public int delete(Long userId, Integer commentType, String movementId) {
        //1.创建条件对象
        Query query = new Query(
                Criteria.where("userId").is(userId)
                .and("commentType").is(commentType)
                .and("publishId").is(new ObjectId(movementId))
        );

        //2.删除指定的记录
        mongoTemplate.remove(query,Comment.class);

        //3.修改指定操作数据,更新动态
        //3.1 查询动态对象
        Movement movement = mongoTemplate.findById(new ObjectId(movementId), Movement.class);

        //3.2 修改操作数量
        if (commentType == 1) {
            //点赞
            movement.setLikeCount(movement.getLikeCount() - 1);
        }else if (commentType == 2){
            //评论
            movement.setCommentCount(movement.getCommentCount() - 1);
        }else if (commentType == 3){
            //喜欢
            movement.setLoveCount(movement.getLoveCount() - 1);
        }

        //3.3 更新动态对象
        mongoTemplate.save(movement);


        //4.返回最新的点赞数
        if (commentType == 1) {
            //点赞
            return  movement.getLikeCount();
        }else if (commentType == 2){
            //评论
            return movement.getCommentCount();
        }else if (commentType == 3){
            //喜欢
            return movement.getLoveCount();
        }
        return 0;
    }

    @Override
    public PageBeanVo findMovementCommentByPage(int pageNum, int pageSize, String movementId) {
        //1.创建条件查询对象(动态id,分页,排序)
        Query query = new Query(
                Criteria.where("publishId").is(new ObjectId(movementId))//动态id
                .and("commentType").is(2)//只查询评论
        ).skip((pageNum-1)*pageSize).limit(pageSize)//分页
        .with(Sort.by(Sort.Order.desc("created")));//排序

        //2.查询评论表获取集合
        List<Comment> comments = mongoTemplate.find(query, Comment.class);

        //3.查询总记录数
        long count = mongoTemplate.count(query, Comment.class);

        //4.返回分页对象
        return new PageBeanVo(pageNum,pageSize,count,comments);
    }

    @Override
    public PageBeanVo findByPage(int pageNum, int pageSize, int commentType, Long userId) {
        //1.创建条件对象(类型,userId,分页,排序)
        Query query = new Query(
                Criteria.where("publishUserId").is(userId)
                .and("commentType").is(commentType)
        ).skip((pageNum-1)*pageSize).limit(pageSize).with(Sort.by(Sort.Order.desc("created")));

        //2.查询评论表
        List<Comment> comments = mongoTemplate.find(query, Comment.class);

        //3.查询总记录数
        long count = mongoTemplate.count(query, Comment.class);

        //4.封装pageBeanVo且返回
        return new PageBeanVo(pageNum,pageSize,count,comments);
    }


}
