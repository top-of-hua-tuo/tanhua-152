package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.AnswerRecords;
import com.itheima.service.mongo.AnswerRecordsService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

@Service
public class AnswerRecordsServiceImpl implements AnswerRecordsService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void save(AnswerRecords answerRecords) {
        mongoTemplate.save(answerRecords);
    }
}
