package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.ReportIdentify;
import com.itheima.service.mongo.ReportIdentifyService;
import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class ReportIdentifyServiceImpl implements ReportIdentifyService {
    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public ReportIdentify findById(ObjectId id) {
        return mongoTemplate.findById(id, ReportIdentify.class);
    }

    @Override
    public List<ReportIdentify> findAll() {
        return mongoTemplate.find(new Query(), ReportIdentify.class);
    }


}
