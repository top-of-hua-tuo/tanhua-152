package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Movement;
import com.itheima.service.mongo.MessageService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MongoTemplate mongoTemplate;
    //通过审核
    @Override
    public void pass(String movementId) {
        //创建查询对象
        Query query = new Query(Criteria.where("_id").is(movementId));
        Update update = new Update();
        //更新对象
        update.set("state", 1);
        mongoTemplate.updateFirst(query, update, Movement.class);
    }
    //拒绝审核
    @Override
    public void reject(String movementId) {
        //创建查询对象
        Query query = new Query(Criteria.where("_id").is(movementId));
        Update update = new Update();
        //更新对象
        update.set("state", 2);
        mongoTemplate.updateFirst(query, update, Movement.class);

    }
}
