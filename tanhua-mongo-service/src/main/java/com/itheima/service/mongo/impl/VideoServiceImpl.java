package com.itheima.service.mongo.impl;

import cn.hutool.core.collection.CollUtil;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.RecommendVideo;
import com.itheima.domain.mongo.Video;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.service.mongo.VideoService;
import com.itheima.util.ConstantUtil;
import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@Service //dubbo
public class VideoServiceImpl implements VideoService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IdService idService;

    @Override
    public PageBeanVo findByPage(int pageNum, int pageSize, Long userId) {
        //1.创建条件查询对象(用户id,分页,排序)
        Query query = new Query(
                Criteria.where("userId").is(userId)
        ).skip((pageNum-1)*pageSize).limit(pageSize).with(Sort.by(Sort.Order.desc("date")));

        //2.查询推荐表(只有视频id)
        List<RecommendVideo> recommendVideos = mongoTemplate.find(query, RecommendVideo.class);

        //3.遍历推荐表集合,通过视频id查询视频表获取详情,放入一个新集合中
        //3.0 创建VideoList
        List<Video> videoList = new ArrayList<>();

        //3.1 遍历recommendVideos
        if (CollUtil.isNotEmpty(recommendVideos)) {
            for (RecommendVideo recommendVideo : recommendVideos) {
                //3.2 通过视频id查询视频详情
                Video video = mongoTemplate.findById(recommendVideo.getVideoId(), Video.class);

                //3.3 放入VideoList中
                videoList.add(video);
            }
        }

        //4.查询总记录数
        long count = mongoTemplate.count(query, RecommendVideo.class);

        //5.返回pageBeanVo
        return new PageBeanVo(pageNum,pageSize,count,videoList);
    }

    @Override
    public void save(Video video) {
        //1.往视频表中保存
        //给视频设置一个vid,给大数据团队使用
        Long vid = idService.getNextId(ConstantUtil.VIDEO_ID);
        video.setVid(vid);

        mongoTemplate.save(video);

        //2.往推荐表中保存(推荐给自己)
        RecommendVideo recommendVideo = new RecommendVideo();

        recommendVideo.setDate(System.currentTimeMillis());
        recommendVideo.setVid(vid);
        recommendVideo.setUserId(video.getUserId());//推荐给自己
        recommendVideo.setVideoId(video.getId());
        recommendVideo.setScore(100D);

        mongoTemplate.save(recommendVideo);
    }

    @Override
    public PageBeanVo findUserVideoByPage(int pageNum, int pageSize, Long userId) {
        //1.创建条件对象(用户id,分页,排序)
        Query query = new Query(Criteria.where("userId").is(userId))
                .skip((pageNum-1)*pageSize).limit(pageSize)
                .with(Sort.by(Sort.Order.desc("created")));

        //2.查询视频表
        List<Video> videoList = mongoTemplate.find(query, Video.class);

        //3.查询总记录数
        long count = mongoTemplate.count(query, Video.class);

        //4.封装pageBeanVo
        return new PageBeanVo(pageNum,pageSize,count,videoList);
    }

    //视频点赞
    @Override
    public int saveCommentLike(Comment comment) {

        //获取视频id
        ObjectId publishId = comment.getPublishId();

        //获取视频
        Video video = mongoTemplate.findById(publishId, Video.class);

        //设置操作对象所属id
        comment.setPublishUserId(video.getUserId());

        //保存到评论表中
        mongoTemplate.save(comment);

        //修改视频表中指定类型的数量
        Integer commentType = comment.getCommentType();
        if(commentType==4){
            //视频点赞
            video.setLikeCount(video.getLikeCount()+1);
        }else if(commentType==5){
            //视频评论
            video.setCommentCount(video.getCommentCount()+1);
        }
        mongoTemplate.save(video);

        //返回指定操作的数量
        if(commentType==4){
            return video.getLikeCount();
        }else if(commentType==5){
            return video.getCommentCount();
        }
        return 0;
    }

    //视频评论列表
    @Override
    public PageBeanVo findVideoCommentByPage(int pageNum, int pageSize, String videoId) {
        //创建条件
        Query query = new Query(Criteria.where("publishId").is(new ObjectId(videoId))
                                    .and("commentType").is(5)).skip((pageNum-1)*pageSize).limit(pageSize)
                                    .with(Sort.by(Sort.Order.desc("created")));
        //查询评论表获取集合
        List<Comment> comments = mongoTemplate.find(query, Comment.class);

        //查询总记录数
        long count = mongoTemplate.count(query, Comment.class);

        return new PageBeanVo(pageNum,pageSize,count,comments);
    }
}
