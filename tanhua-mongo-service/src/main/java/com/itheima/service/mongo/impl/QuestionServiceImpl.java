package com.itheima.service.mongo.impl;

import cn.hutool.core.util.StrUtil;
import com.itheima.domain.mongo.Question;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.service.mongo.QuestionService;
import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    IdService idService;



    @Override
    public Question findByQuestionId(Long questionId) {
        //根据questionId查询返回
        return mongoTemplate.findOne(Query.query(Criteria.where("qid").is(questionId)), Question.class);
    }

    @Override
    public PageBeanVo findByPage(Integer pageNum, Integer pageSize, String level, String keyword) {
        //1.创建分页查询对象 分页查询
        Query query = new Query().skip((pageNum-1)*pageSize).limit(pageSize);
        if (StrUtil.isNotBlank(level)) {
            query.addCriteria(Criteria.where("level").is(level));
        }
        if (StrUtil.isNotBlank(keyword)) {
            query.addCriteria(Criteria.where("question").is(keyword));
        }
        //2.查询
        List<Question> questionList = mongoTemplate.find(query, Question.class);
        //3.查询总记录数
        long count = mongoTemplate.count(query, Question.class);
        return new PageBeanVo(pageNum, pageSize, count, questionList);
    }

    @Override
    public Long save(Question question) {
        //1.获取全局id
        Long qid = idService.getNextId("question");
        question.setQid(qid);
        mongoTemplate.save(question);
        return qid;
    }

    @Override
    public void update(Question question) {
        //1.从数据库查询后设置id修改
        Question question2 = mongoTemplate.findOne(Query.query(Criteria.where("qid").is(question.getQid())), Question.class);
        ObjectId id = question2.getId();

        question.setId(id);
        mongoTemplate.save(question);
    }
}
