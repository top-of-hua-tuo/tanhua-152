package com.itheima.service.mongo.impl;


import com.itheima.domain.mongo.UserStatusLog;
import com.itheima.service.mongo.UserStatusLogService;

import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

@Service
public class UserStatusServiceLogImpl implements UserStatusLogService {

    @Autowired
    MongoTemplate mongoTemplate;


    @Override
    public void save(UserStatusLog userStatusLog) {
        mongoTemplate.save(userStatusLog);
    }

    @Override
    public UserStatusLog findById(ObjectId userStatusLogId) {
        return mongoTemplate.findById(userStatusLogId, UserStatusLog.class);
    }
}
