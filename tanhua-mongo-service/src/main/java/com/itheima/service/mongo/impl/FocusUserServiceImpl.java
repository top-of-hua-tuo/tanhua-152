package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.FocusUser;
import com.itheima.service.mongo.FocusUserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service //dubbo
public class FocusUserServiceImpl implements FocusUserService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void saveFocusUser(FocusUser focusUser) {
        mongoTemplate.save(focusUser);
    }

    @Override
    public void deleteFocusUser(Long userId, Long focusUserId) {
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(userId).and("focusUserId").is(focusUserId)),FocusUser.class);
    }
}
