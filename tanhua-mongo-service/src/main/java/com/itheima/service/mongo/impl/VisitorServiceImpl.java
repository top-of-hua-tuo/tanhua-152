package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Visitor;
import com.itheima.service.mongo.VisitorService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
@Service //dubbo
public class VisitorServiceImpl implements VisitorService{

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Visitor> findVisitorsSinceLastAccessTime(Long userId, Long lastAccessTime) {
        //1.创建条件对象(userid,时间,分页,排序)
        Query query = new Query(
                Criteria.where("userId").is(userId)
                .and("date").gte(lastAccessTime)
        ).skip(0).limit(4).with(Sort.by(Sort.Order.desc("date")));

        //2.查询访客表
        return mongoTemplate.find(query,Visitor.class);
    }
}
