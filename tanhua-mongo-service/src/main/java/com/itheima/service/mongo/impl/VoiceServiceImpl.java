package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Voice;
import com.itheima.service.mongo.VoiceService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class VoiceServiceImpl implements VoiceService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void save(Voice voice) {
            mongoTemplate.save(voice);
    }

    @Override
    public Long getVisibleCount(Long userId) {

        Query query = new Query(
                Criteria.where("visibility").ne(false).and("userId").ne(userId)
        );

        long count = mongoTemplate.count(query, Voice.class);

        return count;
    }

    @Override
    public Voice findVoiceByRandomNumber(Long randomNum,Long userId) {

        Query query = new Query(
                Criteria.where("visibility").ne(false).and("userId").ne(userId)
        ).skip(randomNum).limit(1);

        Voice voice = mongoTemplate.findOne(query, Voice.class);

        return voice;
    }

}
