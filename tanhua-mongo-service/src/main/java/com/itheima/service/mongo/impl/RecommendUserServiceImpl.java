package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.service.mongo.RecommendUserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;


@Service //dubbo
public class RecommendUserServiceImpl implements RecommendUserService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public RecommendUser findTodayBest(Long userId) {
        //1.创建条件对象(id,分页,排序)
        Query query = new Query(
                Criteria.where("toUserId").is(userId)
        ).skip(0).limit(1).with(Sort.by(Sort.Order.desc("score")));


        //2.查询推荐用户表中唯一的数据且返回
        return mongoTemplate.findOne(query,RecommendUser.class);
    }

    @Override
    public PageBeanVo findByPage(int pageNum, int pageSize, Long userId) {
        //1.创建条件对象(userId,分页,排序)
        Query query = new Query(
                Criteria.where("toUserId").is(userId)
        ).skip((pageNum-1)*pageSize+1).limit(pageSize)//去除了今日最佳
                .with(Sort.by(Sort.Order.desc("score")));

        //2.查询推荐好友表
        List<RecommendUser> recommendUsers = mongoTemplate.find(query, RecommendUser.class);

        //3.查询总记录数
        long count = mongoTemplate.count(query, RecommendUser.class);

        //4.封装pageBeanVo返回
        return new PageBeanVo(pageNum,pageSize,count,recommendUsers);
    }

    @Override
    public RecommendUser findPersonal(Long recommendUserId, Long userId) {
        return mongoTemplate.findOne(Query.query(Criteria.where("toUserId").is(userId).and("userId").is(recommendUserId)),RecommendUser.class);
    }
}
