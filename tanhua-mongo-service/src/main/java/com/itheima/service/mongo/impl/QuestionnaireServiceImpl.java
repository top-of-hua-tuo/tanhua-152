package com.itheima.service.mongo.impl;

import com.itheima.domain.mongo.Questionnaire;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.service.mongo.QuestionnaireService;
import org.apache.dubbo.config.annotation.Service;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
@Service
public class QuestionnaireServiceImpl implements QuestionnaireService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Questionnaire> findAll() {
        //1.查询所有问卷返回(考虑分页前三条？)
        return mongoTemplate.find(new Query(), Questionnaire.class);
    }

    @Override
    public void updateByLevel(String level, Long questionIds) {
        //1.根据条件查询
        Questionnaire questionnaire = mongoTemplate.findOne(Query.query(Criteria.where("level").is(level)), Questionnaire.class);
        questionnaire.getQuestionIds().add(questionIds);
        mongoTemplate.save(questionnaire);
    }

    @Override
    public PageBeanVo findByPage(Integer pageNum, Integer pageSize, String level, String keyword) {
        //1.查询所有和记录数返回
        List<Questionnaire> questionnaires = mongoTemplate.find(new Query(), Questionnaire.class);
        long count = mongoTemplate.count(new Query(), Questionnaire.class);

        return new PageBeanVo(pageNum,pageSize,count,questionnaires);
    }

    @Override
    public Questionnaire findByName(String name) {
        return mongoTemplate.findOne(Query.query(Criteria.where("name").is(name)), Questionnaire.class);
    }



}
