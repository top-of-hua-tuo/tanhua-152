package com.itheima.web.controller;

import com.itheima.web.manager.AnalysisManager;
import com.itheima.web.manager.AnalysisServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnalysisController {

    @Autowired
    AnalysisServiceManager analysisServiceManager;

    @GetMapping("/dashboard/users")
    public ResponseEntity findSummary( Long sd,  Long ed, Integer type){
        return analysisServiceManager.users(sd, ed, type);
    }

}
