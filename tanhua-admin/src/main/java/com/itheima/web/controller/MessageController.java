package com.itheima.web.controller;


import com.itheima.service.mongo.MessageService;
import com.itheima.web.manager.MessageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MessageController {
    @Autowired
    MessageManager messageManager;
    //通过审核
    @PostMapping("/manage/messages/pass")
    public ResponseEntity pass(@RequestBody List<String> movementID) {
        //遍历动态
        for (String id : movementID) {
            messageManager.pass(id);
        }
        return ResponseEntity.ok(null);
    }
    //拒绝审核
    @PostMapping("/manage/messages/reject")
    public ResponseEntity reject(@RequestBody List<String> movementID) {
        //遍历动态
        for (String id : movementID) {
            messageManager.reject(id);
        }
        return ResponseEntity.ok(null);
    }
}
