package com.itheima.web.controller;

import com.itheima.domain.vo.PageBeanVo;
import com.itheima.web.manager.QuestionManager;
import com.itheima.web.manager.QuestionnaireManager;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class QuestionnaireController {
    @Autowired
    QuestionnaireManager questionnaireManager;

    //1.查询所有试题
    @GetMapping("/questionnaire/findByPage")
    public PageBeanVo findByPage(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                                 @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                                 String level,
                                 String keyword) {
        return questionnaireManager.findByPage(pageNum, pageSize, level, keyword);
    }



    //3.查询单个操作
    @GetMapping("/questionnaire/findByName")
    public ResponseEntity findById(String name) {
        return questionnaireManager.findByName(name);

    }


}
