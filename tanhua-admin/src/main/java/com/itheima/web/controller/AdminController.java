package com.itheima.web.controller;

import cn.hutool.captcha.LineCaptcha;
import com.itheima.web.interceptor.AdminHolder;
import com.itheima.web.manager.AdminManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
public class AdminController {

    @Autowired
    AdminManager adminManager;

    //生成验证码图片
    @GetMapping("/system/users/verification")
    public void getVerification(String uuid, HttpServletResponse response) throws IOException {
        //调用manager生成验证码
       LineCaptcha lineCaptcha = adminManager.getVerification(uuid);

       //将验证码图片写回浏览器
        lineCaptcha.write(response.getOutputStream());
    }

    @PostMapping("/system/users/login")
    public ResponseEntity login(@RequestBody Map<String,String> map){
        //1.获取四个参数
        String username = map.get("username");
        String password = map.get("password");
        String verificationCode = map.get("verificationCode");
        String uuid = map.get("uuid");

        //2.调用manager完成登陆
        return adminManager.login(username,password,verificationCode,uuid);
    }

    @PostMapping("/system/users/profile")
    public ResponseEntity findAdminInfo(@RequestHeader("Authorization") String token){
        //return adminManager.findAdminInfo(token);
        return ResponseEntity.ok(AdminHolder.get());
    }

    @PostMapping("/system/users/logout")
    public ResponseEntity logout(@RequestHeader("Authorization") String token){
        return adminManager.logout(token);
    }
}
