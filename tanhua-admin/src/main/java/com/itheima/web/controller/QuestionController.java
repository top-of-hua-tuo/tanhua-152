package com.itheima.web.controller;

import com.itheima.domain.vo.PageBeanVo;
import com.itheima.web.manager.QuestionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class QuestionController {
    @Autowired
    QuestionManager questionManager;

    //1.查询所有试题
    @GetMapping("/question/findByPage")
    public PageBeanVo findByPage(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                                 @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                                 String level,
                                 String keyword) {
        return questionManager.findByPage(pageNum, pageSize, level, keyword);
    }

    //2.保存操作
    @PostMapping("/question/save")
    public ResponseEntity save(@RequestBody Map<String,String> map) {

        return questionManager.save(map);
    }

    //3.查询单个操作
    @GetMapping("/question/findByPid")
    public ResponseEntity findByPid(Long pid) {
        return questionManager.findByPid(pid);
    }

    //4.修改操作
    @PostMapping("/question/update")
    public ResponseEntity update(@RequestBody Map map) {
        return questionManager.update(map);
    }

}
