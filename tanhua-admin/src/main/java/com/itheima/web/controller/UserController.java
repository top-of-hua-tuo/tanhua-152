package com.itheima.web.controller;

import cn.hutool.core.util.NumberUtil;
import com.itheima.web.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class UserController {

    @Autowired
    UserManager userManager;

    @GetMapping("/manage/users")
    public ResponseEntity findByPage(
            @RequestParam(value = "page", defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") int pageSize) {

        return userManager.findByPage(pageNum, pageSize);
    }

    @GetMapping("/manage/users/{userId}")
    public ResponseEntity findUserInfoById(@PathVariable Long userId) {
        return userManager.findUserInfoById(userId);
    }

    @GetMapping("/manage/messages")
    public ResponseEntity findMovementByCondition4Page(
            Long uid,
            String state,
            @RequestParam(value = "page", defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") int pageSize) {

        Integer status = NumberUtil.isInteger(state) ? NumberUtil.parseInt(state) : null;

        return userManager.findMovementByCondition4Page(uid, status, pageNum, pageSize);
    }

    @GetMapping("/manage/messages/{movementId}")
    public ResponseEntity findMovementById(@PathVariable String movementId) {
        return userManager.findMovementById(movementId);
    }


    @GetMapping("/manage/messages/comments")
    public ResponseEntity findMovementComemntByPage(
            @RequestParam(value = "page", defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") int pageSize,
            String messageID) {
        return userManager.findMovementComemntByPage(pageNum, pageSize, messageID);
    }

    @GetMapping("/manage/videos")
    public ResponseEntity findUserVideoByPage(
            @RequestParam(value = "page", defaultValue = "1") int pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") int pageSize,
            Long uid) {
        return userManager.findUserVideoByPage(pageNum, pageSize, uid);
    }

    //查询用户地理位置(经纬度)
    @GetMapping("/manage/position")
    public ResponseEntity findPositionByUserId(
            @RequestParam(value = "userId", defaultValue = "1") Long userId
    ) {
        //调用manager查询用户的地理坐标
        return userManager.findPositionByUserId(userId);
    }


    //冻结用户
    @PostMapping("/manage/users/freeze")
    public ResponseEntity freezingUser(@RequestBody Map<String, String> map) {
        Long userId = NumberUtil.parseLong(map.get("userId"));
        Integer freezingTime = NumberUtil.parseInt(map.get("freezingTime"));
        Integer freezingRange = NumberUtil.parseInt(map.get("freezingRange"));
        String reasonsForFreezing = map.get("reasonsForFreezing");
        String frozenRemarks = map.get("frozenRemarks");

        return userManager.freezingUser(userId, freezingTime, freezingRange, reasonsForFreezing, frozenRemarks);
    }

    //解冻用户
    @PostMapping("/manage/users/unfreeze")
    public ResponseEntity unfreezingUser(@RequestBody Map<String ,String> map){
        long userId = NumberUtil.parseLong(map.get("userId"));
        String reasonsForThawing = map.get("reasonsForThawing");
        return userManager.unfreezingUser(userId,reasonsForThawing);
    }


}
