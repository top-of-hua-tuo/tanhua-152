package com.itheima.web.listener;

import com.itheima.autoconfig.huawei.HuaWeiUGCTemplate;
import com.itheima.domain.mongo.Movement;
import com.itheima.service.mongo.MovementService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
//@RocketMQMessageListener(consumerGroup = "consumer-movement-group",topic = "tanhua-movement")
public class MovementListener implements RocketMQListener<Movement>{

    @Autowired
    HuaWeiUGCTemplate huaWeiUGCTemplate;

    @Reference
    MovementService movementService;

    @Override
    public void onMessage(Movement movement) {
        //0.重新查询movement(主要是为了获取pid)
        movement = movementService.findById(movement.getId().toString());

        //1.获取动态的内容和图片
        String textContent = movement.getTextContent();
        String[] imgArr = movement.getMedias().toArray(new String[]{});

        //2.审核
        boolean textResult = huaWeiUGCTemplate.textContentCheck(textContent);
        boolean imgResult = huaWeiUGCTemplate.imageContentCheck(imgArr);

        if (textResult && imgResult) {
            //3.若审核通过,修改动态的状态1
            movement.setState(1);
            System.out.println(movement.getId()+"审核通过了");
        }else {
            //4.若审核不过,修改动态的状态2
            movement.setState(2);
            System.out.println(movement.getId()+"审核没通过");
        }

        //5.更新动态对象
        movementService.update(movement);

    }
}
