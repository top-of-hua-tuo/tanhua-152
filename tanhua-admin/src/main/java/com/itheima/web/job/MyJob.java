package com.itheima.web.job;

import com.itheima.service.db.AnalysisByDayService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MyJob {

    @Reference
    AnalysisByDayService analysisByDayService;


    @Scheduled(cron = "0 0 0/1 * * ?")//每小时
    //@Scheduled(cron = "0 0/1 * * * ?")//每分钟
    public void saveOrUpdateAnalysis(){
        analysisByDayService.saveOrUpdate();
        System.err.println("今日统计数据已保存或更新....");
    }
}
