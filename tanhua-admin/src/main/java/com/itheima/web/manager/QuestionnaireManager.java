package com.itheima.web.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.itheima.domain.db.MarkSheet;
import com.itheima.domain.mongo.Question;
import com.itheima.domain.mongo.Questionnaire;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.domain.vo.QuestionVoAdmain;
import com.itheima.domain.vo.QuestionnaireVoAdmain;
import com.itheima.service.db.MarkSheetService;
import com.itheima.service.mongo.QuestionService;
import com.itheima.service.mongo.QuestionnaireService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QuestionnaireManager {
    @Reference
    QuestionService questionService;
    @Reference
    QuestionnaireService questionnaireService;
    @Reference
    MarkSheetService markSheetService;


    //1.查询所有
    public PageBeanVo findByPage(Integer pageNum, Integer pageSize, String level, String keyword) {

        //1.查询所有问卷
        PageBeanVo page = questionnaireService.findByPage(pageNum, pageSize, level, keyword);
        //2.获取试题id集合，查询试题
        List<Questionnaire> items = (List<Questionnaire>) page.getItems();


        //封装questionnaireVoAdmain
        List<QuestionnaireVoAdmain> questionnaireVoAdmainList = new ArrayList<>();

        if (CollUtil.isNotEmpty(items)) {
            for (Questionnaire questionnaire : items) {
                List<Map<String, String>> questions = new ArrayList<>();
                Map<String, String> map = new HashMap<>();
                QuestionnaireVoAdmain questionnaireVoAdmain = new QuestionnaireVoAdmain();
                List<Long> questionIds = questionnaire.getQuestionIds();
                if (CollUtil.isNotEmpty(questionIds)) {
                    for (Long questionId : questionIds) {
                        Question question = questionService.findByQuestionId(questionId);
                        map.put(question.getQid().toString(), question.getQuestion());
                    }
                }
                questions.add(map);
                BeanUtil.copyProperties(questionnaire, questionnaireVoAdmain);
                questionnaireVoAdmain.setQuestions(questions);
                questionnaireVoAdmainList.add(questionnaireVoAdmain);
            }
        }
        page.setItems(questionnaireVoAdmainList);
        return page;
    }

    //2.查询单个
    public ResponseEntity findByName(String name) {
        Questionnaire questionnaire = questionnaireService.findByName(name);
        List<Long> questionIds = questionnaire.getQuestionIds();
        List<Map<String, String>> questions = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        if (CollUtil.isNotEmpty(questionIds)) {
            for (Long questionId : questionIds) {
                Question question = questionService.findByQuestionId(questionId);
                map.put(question.getQid().toString(), question.getQuestion());
            }
            questions.add(map);
        }
        QuestionnaireVoAdmain questionnaireVoAdmain = new QuestionnaireVoAdmain();
        BeanUtil.copyProperties(questionnaire, questionnaireVoAdmain);
        questionnaireVoAdmain.setQuestions(questions);
        return ResponseEntity.ok(questionnaireVoAdmain);
    }
}
