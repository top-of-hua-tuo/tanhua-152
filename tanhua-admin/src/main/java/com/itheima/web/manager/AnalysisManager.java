package com.itheima.web.manager;

import com.itheima.service.db.AnalysisByDayService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class AnalysisManager {

    @Reference
    AnalysisByDayService analysisByDayService;

    public ResponseEntity findSummary() {
        return ResponseEntity.ok(analysisByDayService.findSummary());
    }
}
