package com.itheima.web.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.UserStatusLog;
import com.itheima.domain.mongo.Video;
import com.itheima.domain.vo.*;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.*;
import com.itheima.util.ConstantUtil;
import com.itheima.web.interceptor.AdminHolder;
import org.apache.dubbo.config.annotation.Reference;
import org.bson.types.ObjectId;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
public class UserManager {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Reference
    UserInfoService userInfoService;

    @Reference
    MovementService movementService;

    @Reference
    CommentService commentService;

    @Reference
    VideoService videoService;

    @Reference
    UserStatusLogService userStatusLogService;

    @Reference
    UserLocationService userLocationService;

    public ResponseEntity findByPage(int pageNum, int pageSize) {
        PageBeanVo page = userInfoService.findByPage(pageNum, pageSize);
        //获取用户信息，遍历封装添加状态
        List<UserInfo> userInfoList = (List<UserInfo>) page.getItems();
        List<UserInfoAndStatusVo> userInfoAndStatusVoList = new ArrayList<>();

        for (UserInfo userInfo : userInfoList) {
            Long userId = userInfo.getId();
            String userStatus = stringRedisTemplate.hasKey(ConstantUtil.BeFreezing_user + userId) ? "2" : "1";
            UserInfoAndStatusVo userInfoAndStatusVo = new UserInfoAndStatusVo();
            userInfoAndStatusVo.setUserInfo(userInfo);
            userInfoAndStatusVo.setUserStatus(userStatus);
            userInfoAndStatusVoList.add(userInfoAndStatusVo);
        }

        page.setItems(userInfoAndStatusVoList);

        return ResponseEntity.ok(page);
    }

    public ResponseEntity findUserInfoById(Long userId) {
        UserInfo userInfo = userInfoService.findById(userId);
        UserInfoAndStatusVo userInfoAndStatusVo = new UserInfoAndStatusVo();
        userInfoAndStatusVo.setUserInfo(userInfo);
        String userStatus = stringRedisTemplate.hasKey(ConstantUtil.BeFreezing_user + userId) ? "2" : "1";
        userInfoAndStatusVo.setUserStatus(userStatus);
        return ResponseEntity.ok(userInfoAndStatusVo);
    }

    public ResponseEntity findMovementByCondition4Page(Long uid, Integer state, int pageNum, int pageSize) {
        //1.调用service分页查询动态数据
        PageBeanVo page = movementService.findMovementByCondition4Page(uid, state, pageNum, pageSize);

        //2.若uid不为空,查询用户的userInfo
        UserInfo userInfo = null;
        if (uid != null) {
            userInfo = userInfoService.findById(uid);
        }

        //3.获取分页中动态集合,遍历集合,获取每个动态,查询用户的userInfo,封装MovementVo,放入新集合中
        //3.0 创建MovementVoList
        List<MovementVo> movementVoList = new ArrayList<>();

        //3.1 遍历分页中的集合
        List<Movement> items = (List<Movement>) page.getItems();
        if (CollUtil.isNotEmpty(items)) {
            for (Movement movement : items) {
                //3.2 获取每个动态,查询用户的userInfo,
                if (uid == null) {
                    userInfo = userInfoService.findById(movement.getUserId());
                }

                //3.3 封装MovementVo,
                MovementVo movementVo = new MovementVo();

                //先封装用户,再封装动态
                movementVo.setUserInfo(userInfo);
                movementVo.setMovement(movement);

                //单独处理发布时间
                movementVo.setCreateDate(DateUtil.formatDateTime(new Date(movement.getCreated())));

                //3.4 放入新集合中
                movementVoList.add(movementVo);
            }
        }

        //4.给分页对象设置新集合
        page.setItems(movementVoList);
        //5.返回分页对象
        return ResponseEntity.ok(page);
    }

    public ResponseEntity findMovementById(String movementId) {
        //1.调用service根据id进行查询
        Movement movement = movementService.findById(movementId);

        //2.封装movementVo,且返回
        UserInfo userInfo = userInfoService.findById(movement.getUserId());

        MovementVo movementVo = new MovementVo();
        //先封装用户,再封装动态
        movementVo.setUserInfo(userInfo);
        movementVo.setMovement(movement);
        //单独处理发布时间
        movementVo.setCreateDate(DateUtil.formatDateTime(new Date(movement.getCreated())));

        return ResponseEntity.ok(movementVo);
    }

    public ResponseEntity findMovementComemntByPage(int pageNum, int pageSize, String messageID) {
        //1.调用commentService分页查询
        PageBeanVo page = commentService.findMovementCommentByPage(pageNum, pageSize, messageID);

        //2.获取评论集合,遍历,获取每个评论发布人信息,封装成CommentVo,放入一个新集合中
        //2.0 创建CommentVoList
        List<CommentVo> commentVoList = new ArrayList<>();

        //2.1 遍历评论集合
        List<Comment> items = (List<Comment>) page.getItems();
        if (CollUtil.isNotEmpty(items)) {
            for (Comment comment : items) {
                //2.2 获取每个评论发布人信息
                UserInfo userInfo = userInfoService.findById(comment.getUserId());

                //2.3 封装成CommentVo,
                CommentVo commentVo = new CommentVo();
                commentVo.setAvatar(userInfo.getAvatar());
                commentVo.setNickname(userInfo.getNickname());

                commentVo.setId(comment.getId().toString());
                commentVo.setContent(comment.getContent());
                commentVo.setCreateDate(DateUtil.format(new Date(comment.getCreated()), "yyyy-MM-dd HH:mm:ss"));

                //2.4 放入一个新集合中
                commentVoList.add(commentVo);
            }
        }

        //3.将新集合设置给分页对象且返回
        page.setItems(commentVoList);
        return ResponseEntity.ok(page);
    }

    public ResponseEntity findUserVideoByPage(int pageNum, int pageSize, Long uid) {
        //1.调用service完成分页查询
        PageBeanVo page = videoService.findUserVideoByPage(pageNum, pageSize, uid);

        //2.查询用户的userInfo
        UserInfo userInfo = userInfoService.findById(uid);

        //3.获取视频集合,遍历它,封装videoVo对象,放入新集合中
        List<VideoVo> videoVoList = new ArrayList<>();
        List<Video> items = (List<Video>) page.getItems();

        if (CollUtil.isNotEmpty(items)) {
            for (Video video : items) {
                VideoVo videoVo = new VideoVo();

                //先封装用户,再封装视频
                BeanUtil.copyProperties(userInfo, videoVo);
                BeanUtil.copyProperties(video, videoVo);

                //封装其他字段
                videoVo.setUserId(video.getUserId());
                videoVo.setCover(video.getPicUrl());//封面
                videoVo.setSignature(video.getText());//视频文字说明

                videoVoList.add(videoVo);
            }
        }

        //4.将新集合设置到分页对象中且返回
        page.setItems(videoVoList);

        return ResponseEntity.ok(page);
    }


    //冻结用户
    public ResponseEntity freezingUser(Long userId, Integer freezingTime, Integer freezingRange, String reasonsForFreezing, String frozenRemarks) {
        //获取管理员ID
        Long adminId = AdminHolder.get().getId();
        //封装冻结用户日志，将操作数据留存至MongoDB中
        UserStatusLog userStatusLog = new UserStatusLog();
        userStatusLog.setId(new ObjectId());
        userStatusLog.setCreated(System.currentTimeMillis());
        userStatusLog.setAdminId(adminId);
        userStatusLog.setUserId(userId);
        userStatusLog.setFreezingTime(freezingTime);
        userStatusLog.setFreezingRange(freezingRange);
        userStatusLog.setReasonsForFreezing(reasonsForFreezing);
        userStatusLog.setFrozenRemarks(frozenRemarks);
        userStatusLog.setFreezingByMistake(ConstantUtil.FREEZING_BY_MISTAKE_FALSE);
        userStatusLogService.save(userStatusLog);
        //将冻结状态设置时效性存入Redis中
        String userStatusLogId = userStatusLog.getId().toString();
        stringRedisTemplate.opsForValue().set(ConstantUtil.BeFreezing_user + userId, userStatusLogId, Duration.ofDays(freezingTime.longValue()));

        return ResponseEntity.ok("ok");
    }

    //解冻用户
    public ResponseEntity unfreezingUser(long userId, String reasonsForThawing) {
        if (stringRedisTemplate.hasKey(ConstantUtil.BeFreezing_user + userId)) {
            //获取冻结用户日志
            String userStatusLogIdStr = stringRedisTemplate.opsForValue().get(ConstantUtil.BeFreezing_user + userId);
            ObjectId userStatusLogId = new ObjectId(userStatusLogIdStr);
            UserStatusLog userStatusLog = userStatusLogService.findById(userStatusLogId);
            //更新冻结用户日志
            userStatusLog.setUpdated(System.currentTimeMillis());
            userStatusLog.setFreezingByMistake(ConstantUtil.FREEZING_BY_MISTAKE_TRUE);
            userStatusLog.setReasonsForFreezingByMistake(reasonsForThawing);
            userStatusLogService.save(userStatusLog);
            //删除Redis中的冻结状态
            stringRedisTemplate.delete(ConstantUtil.BeFreezing_user + userId);
        }
        return ResponseEntity.ok("ok");
    }

    //查询用户地理坐标
    public ResponseEntity findPositionByUserId(Long userId) {
        //调用service查询经纬度坐标
        HashMap<String, Double> map = userLocationService.findLocationByUserId(userId);
        //返回map集合
        return ResponseEntity.ok(map);
    }
}
