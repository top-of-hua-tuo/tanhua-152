package com.itheima.web.manager;

import com.itheima.service.mongo.MessageService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
public class MessageManager {
    @Reference
    MessageService messageService;
    //通过审核
    public void pass(String id) {
        messageService.pass(id);
    }
    //拒绝审核
    public void reject(String id) {
        messageService.reject(id);
    }
}
