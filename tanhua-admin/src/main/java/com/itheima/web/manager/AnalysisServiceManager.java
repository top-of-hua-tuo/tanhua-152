package com.itheima.web.manager;

import com.itheima.domain.vo.AnalysisUsersVo;

import com.itheima.service.db.AnalysisApiService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class AnalysisServiceManager {
    @Reference
    AnalysisApiService analysisApiService;
    public ResponseEntity users(Long sd,Long ed,Integer type){
        AnalysisUsersVo vo = analysisApiService.users(sd,ed,type);
//        System.out.println(vo);
        return ResponseEntity.ok(vo);
    }
}
