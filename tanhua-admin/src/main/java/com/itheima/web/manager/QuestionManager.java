package com.itheima.web.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.itheima.domain.db.MarkSheet;
import com.itheima.domain.mongo.Question;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.domain.vo.QuestionVoAdmain;
import com.itheima.service.db.MarkSheetService;
import com.itheima.service.mongo.QuestionService;
import com.itheima.service.mongo.QuestionnaireService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QuestionManager {
    @Reference
    QuestionService questionService;
    @Reference
    QuestionnaireService questionnaireService;
    @Reference
    MarkSheetService markSheetService;


    //1.查询所有
    public PageBeanVo findByPage(Integer pageNum, Integer pageSize, String level, String keyword) {
        return questionService.findByPage(pageNum, pageSize, level, keyword);
    }

    //2.保存操作
    public ResponseEntity save(Map<String,String> map) {
        //1.获取前端参数
        String questionStr = map.get("question");
        String level = map.get("level");
        String questionA =  map.get("questionA");
        String questionB = map.get("questionB");
        String questionC =  map.get("questionC");
        String questionD =  map.get("questionD");
        String questionE =  map.get("questionE");
        String questionF = map.get("questionF");
        String questionG =  map.get("questionG");
        Integer A = NumberUtil.isInteger(map.get("scoreA")) ? NumberUtil.parseInt(map.get("scoreA")) : null;
        Integer B = NumberUtil.isInteger(map.get("scoreB")) ? NumberUtil.parseInt(map.get("scoreB")) : null;
        Integer C = NumberUtil.isInteger(map.get("scoreC")) ? NumberUtil.parseInt(map.get("scoreC")) : null;
        Integer D = NumberUtil.isInteger(map.get("scoreD")) ? NumberUtil.parseInt(map.get("scoreD")) : null;
        Integer E = NumberUtil.isInteger(map.get("scoreE")) ? NumberUtil.parseInt(map.get("scoreE")) : null;
        Integer F = NumberUtil.isInteger(map.get("scoreF")) ? NumberUtil.parseInt(map.get("scoreF")) : null;
        Integer G = NumberUtil.isInteger(map.get("scoreG")) ? NumberUtil.parseInt(map.get("scoreG")) : null;

        //2.封装question保存
        Question question = new Question();
        MarkSheet markSheet = new MarkSheet();
        question.setQuestion(questionStr);
        question.setLevel(level);
        List<Map<String, String>> options = new ArrayList<>();
        Map<String, String> option = new HashMap();
        if (StrUtil.isNotBlank(questionA)) {
            option.put("A", questionA);
            markSheet.setA(A);
        }
        if (StrUtil.isNotBlank(questionB)) {
            option.put("B", questionB);
            markSheet.setB(B);
        }
        if (StrUtil.isNotBlank(questionC)) {
            option.put("C", questionC);
            markSheet.setC(C);
        }
        if (StrUtil.isNotBlank(questionD)) {
            option.put("D", questionD);
            markSheet.setD(D);
        }
        if (StrUtil.isNotBlank(questionE)) {
            option.put("E", questionE);
            markSheet.setE(E);
        }
        if (StrUtil.isNotBlank(questionF)) {
            option.put("F", questionF);
            markSheet.setF(F);
        }
        if (StrUtil.isNotBlank(questionG)) {
            option.put("G", questionG);
            markSheet.setG(G);
        }
        options.add(option);
        question.setOptions(options);
        Long questionIds = questionService.save(question);
        //3.调用问卷表保存试题id
        questionnaireService.updateByLevel(level, questionIds);
        markSheet.setId( questionIds);
        //4.mysql保存评分表
        markSheetService.save(markSheet);

        return ResponseEntity.ok(null);
    }
    //查询单个试题
    public ResponseEntity findByPid(Long pid) {
        //1.查询试题
        Question question = questionService.findByQuestionId(pid);
        //2.查询mysql中对应的分数
        List<Map<String, String>> scores = new ArrayList<>();
        Long qid = question.getQid();
        List<Map<String, String>> options = question.getOptions();
        Map<String, String> map = new HashMap<>();
        if (CollUtil.isNotEmpty(options)) {
            for (Map<String, String> option : options) {
                Set<String> keySet = option.keySet();
                for (String key : keySet) {
                    Integer score = markSheetService.findScore(qid.toString(), key);
                    if (score!=null){
                        map.put(key, score.toString());
                    }

                }
            }
        }
        scores.add(map);
        //3.封装questionVoAdmain
        QuestionVoAdmain questionVoAdmain = new QuestionVoAdmain();
        BeanUtil.copyProperties(question, questionVoAdmain);
        questionVoAdmain.setScores(scores);

        return ResponseEntity.ok(questionVoAdmain);
    }

    public ResponseEntity update(Map map) {
        //1.获取前端参数
        String questionStr = (String) map.get("question");
        String level = (String) map.get("level");
        Long qid =  NumberUtil.parseLong(map.get("qid").toString());
        List<Map<String, String>> options = (List<Map<String, String>>) map.get("options");
        List<Map<String, String>> scores = (List<Map<String, String>>) map.get("scores");

        //2.封装question保存
        Question question = new Question();
        question.setQid(qid);
        question.setQuestion(questionStr);
        question.setLevel(level);
        question.setOptions(options);

        questionService.update(question);

        //3.调用问卷表保存试题id
        MarkSheet markSheet = new MarkSheet();
        markSheet.setId(qid);
        //解析scores 后修改操作
        if (CollUtil.isNotEmpty(scores)) {
            for (Map<String, String> score : scores) {
                for (String key : score.keySet()) {
                    if ("A".equals(key)) {
                        markSheet.setA(NumberUtil.parseInt(score.get(key)));
                    } else if ("B".equals(key)) {
                        markSheet.setB(NumberUtil.parseInt(score.get(key)));
                    }else if ("C".equals(key)) {
                        markSheet.setC(NumberUtil.parseInt(score.get(key)));
                    }else if ("D".equals(key)) {
                        markSheet.setD(NumberUtil.parseInt(score.get(key)));
                    }else if ("E".equals(key)) {
                        markSheet.setE(NumberUtil.parseInt(score.get(key)));
                    }else if ("F".equals(key)) {
                        markSheet.setF(NumberUtil.parseInt(score.get(key)));
                    }else if ("G".equals(key)) {
                        markSheet.setG(NumberUtil.parseInt(score.get(key)));
                    }
                }
            }
        }
        //4.mysql修改评分表
        markSheetService.update(markSheet);

        return ResponseEntity.ok(null);
    }
}
