package com.itheima.web.config;

import com.itheima.web.interceptor.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    TokenInterceptor tokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/system/users/login","/system/users/verification",
                        "/manage/users","/manage/position","/question/findByPage",
                        "/question/save","/question/findByPid","/question/update","/questionnaire/findByPage","/questionnaire/findByName"); //暂时把这两个接口放开
                //放开接口

    }
}
