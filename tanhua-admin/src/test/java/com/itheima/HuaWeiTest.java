package com.itheima;

import com.itheima.autoconfig.huawei.HuaWeiUGCTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HuaWeiTest {

    @Autowired
    private HuaWeiUGCTemplate template;

    @Test
    public void testText() {
        boolean check = template.textContentCheck("大家以后要想成为IT精英,现在就需要加紧学习,TMD");
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.err.println(check);

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
    }

    @Test
    public void testImages() {
        String[] urls = new String[]{
                "http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/9.jpg",
                "http://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/logo/10.jpg",
                "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp2.cri.cn%2FM00%2FA9%2F5A%2FCqgNOls6KliAcnhfAAAAAAAAAAA642.550x412.241x180.jpg&refer=http%3A%2F%2Fp2.cri.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628218957&t=1d2d4e3be7201b35862a29ea44a9b05c"
        };
        boolean check = template.imageContentCheck(urls);
        System.out.println(check);
    }
}