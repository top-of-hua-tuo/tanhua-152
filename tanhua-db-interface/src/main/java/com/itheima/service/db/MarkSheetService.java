package com.itheima.service.db;

import com.itheima.domain.db.MarkSheet;

public interface MarkSheetService {

    //条件查询分数
    Integer findScore(String questionId, String optionId);

    void save(MarkSheet markSheet);

    void update(MarkSheet markSheet);
}
