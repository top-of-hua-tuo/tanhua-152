package com.itheima.service.db;

import com.itheima.domain.vo.AnalysisUsersVo;

public interface AnalysisApiService {
    // 新增、活跃用户、次日留存率
    AnalysisUsersVo users(Long sd, Long ed, Integer type);
}
