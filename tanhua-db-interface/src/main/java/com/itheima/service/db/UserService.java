package com.itheima.service.db;

import com.itheima.domain.db.User;

public interface UserService {
    Long save(User user);

    User findByPhone(String phone);


    void updatePhone(User user);
}
