package com.itheima.service.db;

import com.itheima.domain.vo.AnalysisSummaryVo;

public interface AnalysisByDayService {
    void saveOrUpdate();

    AnalysisSummaryVo findSummary();
}
