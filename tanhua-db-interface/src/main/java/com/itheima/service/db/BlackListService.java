package com.itheima.service.db;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.UserInfo;

public interface BlackListService {
    Page<UserInfo> findByPage(int pageNum, int pageSize, Long userId);
    void deleteByUserIdAndBlackId(Long userId,Long blackId);
}
