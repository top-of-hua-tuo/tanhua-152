package com.itheima.service.db;

import com.itheima.domain.db.UserInfo;
import com.itheima.domain.vo.PageBeanVo;

public interface UserInfoService {
    void save(UserInfo userInfo);
    void update(UserInfo userInfo);
    UserInfo findById(Long id);

    PageBeanVo findByPage(int pageNum,int pageSize);
}
