package com.itheima.service.db;

import com.itheima.domain.vo.PageBeanVo;

public interface AnnouncementService {
    PageBeanVo findAll(int pageNum, int pageSize);
}
