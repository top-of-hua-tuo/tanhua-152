package com.itheima.service.db;

import com.itheima.domain.db.Admin;

public interface AdminService {
    Admin findByUsername(String username);
}
