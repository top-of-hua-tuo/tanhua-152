package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.AnalysisByDay;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface AnalysisByDayMapper extends BaseMapper<AnalysisByDay> {
    //查询累计用户数
    @Select("SELECT SUM(num_registered) FROM tb_analysis_by_day")
    Long findUserSum();

    //查询指定范围内容的活跃用户数
    @Select("SELECT SUM(num_active) FROM  tb_analysis_by_day WHERE record_date BETWEEN #{start} AND #{end}")
    Long findCountWithinPeriod(@Param("start") String start,@Param("end") String end);
}