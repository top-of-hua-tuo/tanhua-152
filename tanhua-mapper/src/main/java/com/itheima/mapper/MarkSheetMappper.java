package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.MarkSheet;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface MarkSheetMappper extends BaseMapper<MarkSheet> {

    //查询分数
    @Select("SELECT ${optionId} FROM tb_mark_sheet WHERE id = #{qid}")
    Integer findScore(@Param("qid") String questionId,@Param("optionId") String optionId);
}
