package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Log;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface LogMapper extends BaseMapper<Log> {

    //查询每日指定类型的人数
    @Select("SELECT COUNT(DISTINCT user_id) FROM tb_log WHERE log_time = #{today} AND TYPE = #{type}")
    int findCountByType(@Param("today") String today,@Param("type") String type);

    //查询每日活跃人数
    @Select("SELECT COUNT(DISTINCT user_id) FROM tb_log WHERE log_time = #{today}")
    int findActiveCount(String today);

    //查询次日留存人数
    @Select("SELECT COUNT(DISTINCT user_id) FROM tb_log WHERE log_time = #{today} " +
            "AND user_id IN (SELECT DISTINCT user_id FROM tb_log WHERE log_time = #{yesterday} AND TYPE = '0102')")
    int findRetention1Day(@Param("today") String today,@Param("yesterday") String yesterday);
}