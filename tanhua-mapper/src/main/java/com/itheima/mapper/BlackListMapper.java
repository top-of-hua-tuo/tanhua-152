package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.BlackList;
import com.itheima.domain.db.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

// 黑名单
public interface BlackListMapper extends BaseMapper<BlackList> {

    @Select("SELECT ui.* FROM tb_black_list bl JOIN tb_user_info ui ON bl.black_user_id = ui.id WHERE bl.user_id = #{userId}")
    Page<UserInfo> findBlackListByUserId(Page<UserInfo> page, @Param("userId") Long userId);
}