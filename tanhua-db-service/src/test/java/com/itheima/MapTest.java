package com.itheima;

import com.itheima.domain.mongo.Question;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MapTest {
    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void testSave() {
        Question questions = new Question();
        questions.setQuestion("尝试就完事了！");
        Map<String, String> option = new HashMap<>();
        option.put("A", "能行么？");
        option.put("B", "看看吧！");
        List< Map<String, String>> options = new ArrayList<>();
        questions.setOptions(options);
        mongoTemplate.save(questions);
    }

}
