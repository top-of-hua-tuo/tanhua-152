package com.itheima.service.db.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.User;
import com.itheima.mapper.UserMapper;
import com.itheima.service.db.UserService;
import com.itheima.util.ConstantUtil;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service //dubbo
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public Long save(User user) {
        //1.设置一个默认密码 123456
        //user.setPassword(ConstantUtil.INIT_PASSWORD);

        //2.对密码进行加密
        user.setPassword(SecureUtil.md5(ConstantUtil.INIT_PASSWORD));

        //3.调用dao完成保存
        userMapper.insert(user);

        //4.返回主键
        return user.getId();
    }

    @Override
    public User findByPhone(String phone) {
        //1.创建条件查询对象
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("phone",phone);

        //2.调用map查询
        return userMapper.selectOne(qw);
    }

    @Override
    public void updatePhone(User user) {
        userMapper.updateById(user);
    }
}
