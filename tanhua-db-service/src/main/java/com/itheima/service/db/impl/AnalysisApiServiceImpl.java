package com.itheima.service.db.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.AnalysisByDay;
import com.itheima.domain.vo.AnalysisUsersVo;
import com.itheima.domain.vo.DataPointVo;
import com.itheima.mapper.AnalysisByDayMapper;

import com.itheima.service.db.AnalysisApiService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
@Service
public class AnalysisApiServiceImpl implements AnalysisApiService {

    @Autowired
    AnalysisByDayMapper analysisByDayMapper;

    @Override
    public AnalysisUsersVo users(Long sd, Long ed, Integer type) {
        AnalysisUsersVo vo = new AnalysisUsersVo();

        // 开始时间
        DateTime startDate = DateUtil.date(sd);
        // 结束时间
        DateTime endDate = DateUtil.date(ed);

        // 今年
        vo.setThisYear(findDataPointVoList(startDate, endDate, type));
        // 去年
        vo.setLastYear(
                findDataPointVoList(
                        DateUtil.offset(startDate, DateField.YEAR, -1),
                        DateUtil.offset(endDate, DateField.YEAR, -1),
                        type)
        );
        return vo;
    }

    // 查询指定日期范围内的统计数据
    private List<DataPointVo> findDataPointVoList(DateTime startDate, DateTime endDate, Integer type) {
        List<DataPointVo> voList = new ArrayList<>();

        String column = null;
        switch (type) {
            case 101: {
                column = "num_registered";
                break;
            }
            case 102: {
                column = "num_active";
                break;
            }
            case 103: {
                column = "num_retention1d";
                break;
            }
        }
        // 自定义sql拼接
        QueryWrapper<AnalysisByDay> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("record_date," + column + " as num_active");
        queryWrapper.ge("record_date", startDate);
        queryWrapper.le("record_date", endDate);
        List<AnalysisByDay> byDayList = analysisByDayMapper.selectList(queryWrapper);
        for (AnalysisByDay byDay : byDayList) {
            DataPointVo vo = new DataPointVo();
            vo.setTitle(DateUtil.format(byDay.getRecordDate(), "yyyy-MM-dd"));
            vo.setAmount(byDay.getNumActive().longValue());
            voList.add(vo);
        }
        return voList;
    }
}