package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Notification;
import com.itheima.mapper.NotificationMapper;
import com.itheima.service.db.NotificationService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service //dubbo
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    NotificationMapper notificationMapper;

    @Override
    public Notification findByUserId(Long userId) {
        QueryWrapper<Notification> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        return notificationMapper.selectOne(qw);
    }

    @Override
    public void save(Notification notification) {
        notificationMapper.insert(notification);
    }

    @Override
    public void update(Notification notification) {
        notificationMapper.updateById(notification);
    }
}
