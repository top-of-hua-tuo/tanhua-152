package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.BlackList;
import com.itheima.domain.db.UserInfo;
import com.itheima.mapper.BlackListMapper;
import com.itheima.service.db.BlackListService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service //dubbo
public class BlackListServiceImpl implements BlackListService {

    @Autowired
    BlackListMapper blackListMapper;

    @Override
    public Page<UserInfo> findByPage(int pageNum, int pageSize, Long userId) {
        //1.开启分页 new Page(..);
        Page<UserInfo> page = new Page<>(pageNum,pageSize);

        //2.调用mapper实现分页查询
        page = blackListMapper.findBlackListByUserId(page,userId);

        //3.返回page对象
        return page;
    }

    @Override
    public void deleteByUserIdAndBlackId(Long userId, Long blackId) {
        //1.创建条件对象
        QueryWrapper<BlackList> qw = new QueryWrapper<>();

        //2.添加条件
        qw.eq("user_id",userId);
        qw.eq("black_user_id",blackId);

        //3.删除
        blackListMapper.delete(qw);
    }
}
