package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.mapper.UserInfoMapper;
import com.itheima.service.db.UserInfoService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service //dubbo注解
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserInfoMapper userInfoMapper;

    @Override
    public void save(UserInfo userInfo) {
        userInfoMapper.insert(userInfo);
    }

    @Override
    public void update(UserInfo userInfo) {
        userInfoMapper.updateById(userInfo);
    }

    @Override
    public UserInfo findById(Long id) {
        return userInfoMapper.selectById(id);
    }

    @Override
    public PageBeanVo findByPage(int pageNum, int pageSize) {
        //1.开启分页(设置分页参数)
        Page<UserInfo> page = new Page<>(pageNum, pageSize);

        //2.执行分页查询
        page = (Page<UserInfo>) userInfoMapper.selectPage(page, null);

        //3.将查询的分页数据封装中pageBeanVo,且返回
        return new PageBeanVo(pageNum,pageSize,page.getTotal(),page.getRecords());
    }
}
