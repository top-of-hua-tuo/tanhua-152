package com.itheima.service.db.impl;

import com.itheima.domain.db.MarkSheet;
import com.itheima.mapper.MarkSheetMappper;
import com.itheima.service.db.MarkSheetService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class MarkSheetServiceImpl implements MarkSheetService {
    @Autowired
    MarkSheetMappper markSheetMappper;

    @Override
    public Integer findScore(String questionId, String optionId) {
        //1.调用自定义方法返回查询分数
        return markSheetMappper.findScore(questionId, optionId);
    }

    @Override
    public void save(MarkSheet markSheet) {
        markSheetMappper.insert(markSheet);
    }

    @Override
    public void update(MarkSheet markSheet) {

        markSheetMappper.updateById(markSheet);
    }
}
