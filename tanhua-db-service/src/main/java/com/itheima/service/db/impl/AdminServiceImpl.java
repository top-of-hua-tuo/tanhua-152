package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Admin;
import com.itheima.mapper.AdminMapper;
import com.itheima.service.db.AdminService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service //dubbo
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminMapper adminMapper;

    @Override
    public Admin findByUsername(String username) {
        //1.创建条件对象
        QueryWrapper<Admin> qw = new QueryWrapper<>();
        qw.eq("username",username);

        //2.执行查询
        return adminMapper.selectOne(qw);
    }
}
