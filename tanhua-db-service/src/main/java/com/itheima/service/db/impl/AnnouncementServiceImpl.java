package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Announcement;
import com.itheima.domain.vo.PageBeanVo;
import com.itheima.mapper.AnnouncementMapper;
import com.itheima.service.db.AnnouncementService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class AnnouncementServiceImpl implements AnnouncementService {
    @Autowired
    AnnouncementMapper announcementMapper;

    @Override
    public PageBeanVo findAll(int pageNum, int pageSize) {
        Page<Announcement> page = new Page<>(pageNum, pageSize);
        page= (Page<Announcement>) announcementMapper.selectPage(page, null);

        return new PageBeanVo(pageNum, pageSize, page.getTotal(), page.getRecords());
    }
}
